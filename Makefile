.DEFAULT_GOAL := dev

init:
	docker network inspect db-network >/dev/null 2>&1 || docker network create --subnet=11.0.2.0/9 --gateway=11.0.2.1 db-network

dev:
	docker-compose -f ./docker/dev.docker-compose.yml --env-file ./env/.env up --build

prod:
	docker-compose -f ./docker/docker-compose.yml --env-file ./env/.env up --build

dev-stop:
	docker-compose -f ./docker/dev.docker-compose.yml --env-file ./env/.env down
