# Key-value database

## Running

Preparing: \
`make init`

Run in prod mode: \
`make prod`

Run in dev mode: \
`make dev`

## Working with admin
Go to: `http://localhost:5001/` \
For connect to master set url: `http://11.0.2.2:5042/api/v1/query` \
For connect to slave set url: `http://11.0.2.3:5042/api/v1/query`

## Docs

1. [Types](docs/types.md)
2. [RabbitMQ](docs/rabbitmq.md)
3. [HTTP API](docs/http.md)
