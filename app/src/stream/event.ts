export enum EVENT {
    TIME_TO_SYNC = 'TIME_TO_SYNC',
    TIME_TO_SNAPSHOT = 'TIME_TO_SNAPSHOT',
    SCHEDULE_SYNC_TASK = 'SCHEDULE_SYNC_TASK',
    SCHEDULE_SNAPSHOT_TASK = 'SCHEDULE_SNAPSHOT_TASK',
}
