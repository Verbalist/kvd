import { Provider } from 'inversify-modules/src/interfaces';
import { DIStreamService, StreamService } from 'src/stream/stream.service';

export const StreamServiceProvider: Provider = {
    provide: DIStreamService,
    useContainer: container =>
        container.bind<StreamService>(DIStreamService).to(StreamService),
};
