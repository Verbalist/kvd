import { module } from 'inversify-modules';
import { DIStreamService } from 'src/stream/stream.service';
import { StreamServiceProvider } from 'src/stream/stream.provider';

@module({
    imports: [],
    exports: [
        DIStreamService,
    ],
    providers: [
        StreamServiceProvider,
    ],
})
export class StreamModule {
}
