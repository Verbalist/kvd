import { injectable } from 'inversify';
import { EventEmitter } from 'events';
import { EVENT } from 'src/stream/event';

export const DIStreamService = Symbol.for('StreamService');

@injectable()
export class StreamService {

    private readonly stream: EventEmitter;

    constructor() {
        this.stream = new EventEmitter();
    }

    emit<T>(event: EVENT, data?: T) {
        this.stream.emit(event, data);
    }

    on(event: EVENT, cb) {
        this.stream.on(event, cb);
    }

    removeListener(event: EVENT, cb) {
        this.stream.removeListener(event, cb);
    }

    init(): void {
        console.log('StreamService init');
    }

    close() {
        this.stream.removeAllListeners();
    }
}
