export type SyncOptionsSchema = {
    timerInterval: number,
    batchLimit: number,
};
