export type SnapshotOptionsSchema = {
    timerInterval: number;
    filename: string;
};
