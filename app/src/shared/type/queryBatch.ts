export type QueryBatchSchema = {
    limit: number,
    offset: number,
};
