export type AddressSchema = {
    host: string;
    port: number;
    protocol: string;
};
