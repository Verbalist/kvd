import { module } from 'inversify-modules';
import { inject } from 'inversify';
import { ConfigModule } from 'src/config/config.module';
import { CoreModule } from 'src/core/core.module';
import { RabbitMQModule } from 'src/transport/rabbitmq/rabbitmq.module';
import { AppService, DIAppService } from 'src/app.service';
import { appServiceProvider } from 'src/app.provider';
import { StoreModule } from 'src/store/store.module';
import { HTTPModule } from 'src/transport/http/http.module';
import { ReplicationModule } from 'src/replication/replication.module';
import { SchedulerModule } from 'src/scheduler/scheduler.module';
import { StreamModule } from 'src/stream/stream.module';

@module({
    imports: [
        ConfigModule,
        StoreModule,
        CoreModule,
        RabbitMQModule,
        HTTPModule,
        StreamModule,
        ReplicationModule,
        SchedulerModule,
    ],
    exports: [],
    providers: [appServiceProvider],
})
export class AppModule {
    constructor(
        @inject(DIAppService) private appService: AppService,
    ) {
    }

    async init(): Promise<boolean> {
        return this.appService.init();
    }

    async close(): Promise<boolean> {
        return this.appService.close();
    }
}
