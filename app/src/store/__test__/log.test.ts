import { DILogService, ILogService } from 'src/store/service/log.service';
import { QuerySchema, QueryType } from 'src/transport/query/query';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { deleteFile, randStr } from 'src/store/__test__/util';
import { createTestContainer } from 'src/__test__/container';

describe('Log service', () => {
    const container = createTestContainer();
    const logService = container.get<ILogService>(DILogService);
    const config = container.get<ConfigService>(DIConfigService);

    test('create and append transactions', async () => {
        const queries: QuerySchema[] = [
            { type: QueryType.get, value: 'key1' },
            { type: QueryType.get, value: ['key1', 'key2', 'key3'] },
            { type: QueryType.get, value: ['key1', 'value1'] },
            { type: QueryType.get, value: ['key1', 322] },
            { type: QueryType.get, value: [['key1', 'value1'], ['key2', 555]] },
        ];
        for (const query of queries) {
            await logService.log(query);
        }

        const res = await logService.read();
        expect(res).toEqual(queries);

    });

    test('create and read log batch', async () => {
        const queries: QuerySchema[] = [...Array(50)].map(() => {
            return { type: QueryType.get, value: randStr(20) };

        });
        for (const query of queries) {
            await logService.log(query);
        }
        expect(await logService.readBatch(5, 5)).toEqual(queries.slice(5, 5 + 5));
        expect(await logService.readBatch(20, 5)).toEqual(queries.slice(20, 20 + 5));
        expect(await logService.readBatch(30, 5)).toEqual(queries.slice(30, 30 + 5));

    });


    afterEach(async () => {
        await deleteFile(`${config.storePath}/log.csv`);
    });

});
