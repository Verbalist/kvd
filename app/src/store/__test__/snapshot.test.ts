import { ModuleBuilder } from 'packages/inversify-modules/src';
import { StoreModule } from 'src/store/store.module';
import { DISnapshotService, ISnapshotService } from 'src/store/service/snapshot.service';
import { deleteFile, generateRandomMemoryStorage, randNumber } from 'src/store/__test__/util';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { Snapshot, Store } from 'src/types';

describe('Snapshot', () => {
    let snapshotService: ISnapshotService;
    let storage: Store;
    let config: ConfigService;
    beforeEach(() => {
        const module = new ModuleBuilder(StoreModule).build();
        snapshotService = module.getProvider<ISnapshotService>(DISnapshotService);
        config = module.getProvider<ConfigService>(DIConfigService);
        storage = generateRandomMemoryStorage(5);
    });

    test('create/read', async () => {
        const randHeight = randNumber();

        await snapshotService.create(randHeight, storage);
        const storeFromCsv = await snapshotService.readLast();
        expect(storeFromCsv!.height).toEqual(randHeight);
        expect(storeFromCsv!.storage).toEqual(storage);
    });

    test('crate and read multiple snapshots', async () => {
        const lastHeight = randNumber();
        const lastStorage = generateRandomMemoryStorage(5);

        await snapshotService.create(randNumber(), generateRandomMemoryStorage(5));
        await snapshotService.create(randNumber(), generateRandomMemoryStorage(5));
        await snapshotService.create(lastHeight, lastStorage);

        const snapshot = await snapshotService.readLast();
        expect(snapshot!.height).toEqual(lastHeight);
        expect(snapshot!.storage).toEqual(lastStorage);
    });

    afterEach(async () => {
        await deleteFile(`${config.storePath}/${config.snapshotOptions.filename}`);
    });

});
