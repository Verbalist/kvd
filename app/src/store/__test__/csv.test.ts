import { ModuleBuilder } from 'packages/inversify-modules/src';
import { deleteFile, generateRandomMemoryStorage, randNumber } from 'src/store/__test__/util';
import { DISnapshotSerialize, ISnapshotSerialize } from 'src/store/service/serialize/snapshot.serialize';
import { CsvFile } from 'src/store/service/csv.service';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { StoreModule } from 'src/store/store.module';
import { DIFsService, FsService } from 'src/store/service/fs.service';
import { createTestContainer } from 'src/__test__/container';
import { DIQueryService, QueryService } from 'src/transport/query/query.service';
import { CoreService, DICoreService } from 'src/core/core.service';

describe('CSV service', () => {
    let serializeService: ISnapshotSerialize;
    let fsService: FsService;
    let config: ConfigService;
    beforeEach(() => {
        const container = createTestContainer();
        serializeService = container.get<ISnapshotSerialize>(DISnapshotSerialize);
        fsService = container.get<FsService>(DIFsService);
        config = container.get<ConfigService>(DIConfigService);
    });

    test('snapshot create', async () => {
        const height = randNumber();
        const storage = generateRandomMemoryStorage(10);

        const rows = serializeService.serialize({ height, storage });
        const file = new CsvFile({
            path: `${config.storePath}/${config.snapshotOptions.filename}`,
        });

        await file.create(rows);
        const res = await file.read();
        const snapshot = serializeService.deserialize(res);
        expect(snapshot.storage).toEqual(storage);
        await deleteFile(`${config.storePath}/${config.snapshotOptions.filename}`);
    });

    test('log create and append', async () => {
        const storage = generateRandomMemoryStorage(10);
        const height = randNumber();

        const rows = serializeService.serialize({ height, storage });

        const file = new CsvFile({
            path: `${config.storePath}/log_test.csv`,
        });

        await file.create(rows.slice(0, 8));
        await file.append(rows.slice(8, 9));
        await file.append(rows.slice(9));
        const res = await file.read();
        const snapshot = serializeService.deserialize(res);
        expect(snapshot.storage).toEqual(storage);
        await deleteFile(`${config.storePath}/log_test.csv`);
    });

});
