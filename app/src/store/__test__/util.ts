import { v4 as uuid } from 'uuid';
import { Store } from 'src/types';
import * as fs from 'fs';

const S = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

export const randStr = (range: number) =>
    Array(range)
    .join()
    .split(',')
    .map(() => S.charAt(Math.floor(Math.random() * S.length)))
    .join('');

export const randNumber = (min: number = 0, max: number = Math.pow(5, 5)): number =>
    max - Math.trunc(Math.random() * (max + (min >= 0 ? min : -1 * min)));

export const generateRandomMemoryStorage = (length: number): Store => {
    const store = new Map<string, string | number>();
    [...Array(length)].forEach((_, i) => {
        i % 2 === 0
            ? store.set(uuid(), randStr(i))
            : store.set(uuid(), randNumber(i * 100_000));
    });
    return store;
};

export const deleteFile = (filePath: string): Promise<void> => {
    return new Promise((res, rej) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                return rej(err);
            }
            return res();
        });
    });
};

