export interface ISerializer<Input, Output> {
    serialize(data: Input): Output;

    deserialize(csv: Buffer): Input;
}
