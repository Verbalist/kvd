import { injectable } from 'inversify';
import { Snapshot, SupportedTypes } from 'src/types';
import { DELIMITER, } from 'src/store/common/constants';
import { ISerializer } from 'src/store/service/serialize/interface';
import { removeLastItemWithDelimiter } from 'src/store/service/serialize/util';
import { Row } from '@fast-csv/format';

export const DISnapshotSerialize = Symbol.for('SnapshotSerialize');

export interface ISnapshotSerialize extends ISerializer<Snapshot, Row[]> {
}

@injectable()
export class SnapshotSerialize implements ISnapshotSerialize {
    serialize(data: Snapshot): Row[] {
        const arrayHash = [this.createHeightRow(data.height), ...data.storage.entries()];
        return arrayHash.map(row => {
            return [JSON.stringify({ value: row })];
        });
    }

    // TODO optimize
    deserialize(csv: Buffer): Snapshot {
        const rows = csv.toString().split(DELIMITER);
        removeLastItemWithDelimiter(rows);
        const rowArrayHash = rows.map((row) => JSON.parse(row).value);
        const height = this.getHeightFromFirstRow(rowArrayHash);
        return {
            height,
            storage: new Map<string, SupportedTypes>(rowArrayHash),
        };
    }

    private createHeightRow(height: number): Row {
        return ['value', height];
    }

    private getHeightFromFirstRow(rows: Row): number {
        const headRow = rows.shift();
        return headRow[1];
    }

}
