import { CsvValueType, SupportedTypes } from 'src/types';

export const insertValueType = (value: SupportedTypes): CsvValueType => {
    return typeof value === 'number' ? CsvValueType.number : CsvValueType.string;
};

export const transformToType = (type: string, value: string): SupportedTypes => {
    return type === '0' ? Number(value) : String(value);
};

// TODO find solution to not create last row delimiter
export const removeLastItemWithDelimiter = (array: string[]) => {
    array.pop();
    return array;
};
