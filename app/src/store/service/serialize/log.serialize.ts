import { injectable } from 'inversify';
import { ISerializer } from 'src/store/service/serialize/interface';
import { QuerySchema } from 'src/transport/query/query';
import { Row } from '@fast-csv/format';
import { DELIMITER } from 'src/store/common/constants';
import { removeLastItemWithDelimiter } from 'src/store/service/serialize/util';

export const DILogSerialize = Symbol.for('LogSerialize');

export interface ILogSerialize extends ISerializer<QuerySchema[], Row> {

}

@injectable()
export class LogSerialize implements ILogSerialize {
    serialize(data: QuerySchema[]): Row {
        const rows = data.map(query => {
            return JSON.stringify({
                type: query.type,
                value: query.value,
            });
        });
        return rows;
    }

    deserialize(csv: Buffer): QuerySchema[] {
        const rows = csv.toString().split(DELIMITER);
        removeLastItemWithDelimiter(rows);
        return rows.map((row) => JSON.parse(row));
    }
}
