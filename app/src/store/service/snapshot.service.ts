import { inject, injectable } from 'inversify';
import { CsvFile } from 'src/store/service/csv.service';
import { Snapshot, Store, SupportedTypes } from 'src/types';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { DISnapshotSerialize, ISnapshotSerialize } from 'src/store/service/serialize/snapshot.serialize';
import { DIFsService, FsService } from 'src/store/service/fs.service';

export const DISnapshotService = Symbol.for('SnapshotService');

export interface ISnapshotService {
    create(height: number, store: Store): Promise<void>;

    readLast(): Promise<Snapshot | undefined>;
}

@injectable()
export class SnapshotService implements ISnapshotService {
    private csv!: CsvFile;

    constructor(
        @inject(DISnapshotSerialize) private readonly serializeService: ISnapshotSerialize,
        @inject(DIConfigService) private readonly config: ConfigService,
        @inject(DIFsService) private readonly fsService: FsService,
    ) {
    }

    init(): void {
        console.log('SnapshotService init');
    }

    async create(height: number, storage: Store): Promise<void> {
        if (!this.isStoreAndHeightValid(height, storage)) {
            console.log('You cant create empty snapshot');
            return;
        }
        this.csv = new CsvFile({
            path: `${this.config.storePath}/${this.config.snapshotOptions.filename}`,
        });
        const serializedStore = this.serializeService.serialize({ height, storage });
        await this.csv.create(serializedStore);
    }

    // TODO ref
    async readLast(): Promise<Snapshot | undefined> {
        try {
            const lastSnapshot = await this.fsService.readLastSnapshot();
            return this.serializeService.deserialize(lastSnapshot);
        } catch (e) {
            console.log('Last snapshot not found', e.message);
            return {
                height: 0,
                storage: new Map<string, SupportedTypes>(),
            };
        }
    }

    private isStoreAndHeightValid(height: number, store: Store): boolean {
        return Boolean(height && store.size);
    }


}
