import { inject, injectable } from 'inversify';
import { CsvFile } from 'src/store/service/csv.service';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { QuerySchema } from 'src/transport/query/query';
import { DILogSerialize, ILogSerialize } from 'src/store/service/serialize/log.serialize';
import { DELIMITER } from 'src/store/common/constants';
import { removeLastItemWithDelimiter } from 'src/store/service/serialize/util';

export const DILogService = Symbol.for('LogService');

export interface ILogService {
    log(query: QuerySchema): Promise<void>;

    read(): Promise<QuerySchema[]>;

    readBatch(offset: number, limit: number): Promise<QuerySchema[]>;
}

@injectable()
export class LogService implements ILogService {
    private readonly csv: CsvFile;

    constructor(
        @inject(DILogSerialize) private readonly serializeService: ILogSerialize,
        @inject(DIConfigService) private readonly config: ConfigService,
    ) {
        this.csv = new CsvFile({
            path: `${this.config.storePath}/log.csv`,
        });
    }

    init(): void {
        console.log('LogService init');
    }

    async log(query: QuerySchema): Promise<void> {
        const row = this.serializeService.serialize([query]);
        await this.csv.append([row]);
    }

    async read(): Promise<QuerySchema[]> {
        const csv = await this.csv.read();
        return this.serializeService.deserialize(csv);
    }

    // TODO temp solution
    async readBatch(offset: number, limit: number): Promise<QuerySchema[]> {
        const rows = (await this.csv.read()).toString().split(DELIMITER);
        removeLastItemWithDelimiter(rows);
        const paginatedRows = rows.slice(offset, offset + limit);
        return paginatedRows.map((row) => JSON.parse(row));
    }

}
