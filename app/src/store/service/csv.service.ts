import { FormatterOptionsArgs, Row, writeToStream } from '@fast-csv/format';
import { createWriteStream, readFile } from 'fs';
import WritableStream = NodeJS.WritableStream;
import { CSV_HEADERS } from 'src/store/common/constants';

type CsvOptions = {
    path: string;
    writeOptions?: FormatterOptionsArgs<Row, Row>;
};

export class CsvFile {
    private readonly headers: string[];
    readonly path: string;
    private readonly writeOptions: FormatterOptionsArgs<Row, Row>;

    constructor(
        private readonly options: CsvOptions,
    ) {
        this.headers = CSV_HEADERS;
        this.path = options.path;
        this.writeOptions = {
            headers: CSV_HEADERS,
            includeEndRowDelimiter: true,
            writeHeaders: false,
            quote: false,
        };
    }

    init(): void {
        console.log('CsvFile init');
    }

    create(rows: Row[]): Promise<void> {
        return this.write(createWriteStream(this.path), rows);
    }

    write(stream: WritableStream, rows: Row[]): Promise<void> {
        return new Promise((res, rej) => {
            writeToStream(stream, rows, this.writeOptions)
            .on('error', (err: Error) => rej(err))
            .on('finish', () => res());
        });
    }

    read(): Promise<Buffer> {
        return new Promise((res, rej) => {
            readFile(this.path, (err, contents) => {
                if (err) {
                    return rej(err);
                }
                return res(contents);
            });
        });
    }

    append(rows: Row[]): Promise<void> {
        return this.write(createWriteStream(this.path, { flags: 'a' }), rows);
    }


}
