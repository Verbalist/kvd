import { inject, injectable } from 'inversify';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import * as fs from 'fs';
import * as path from 'path';

export const DIFsService = Symbol.for('FsService');

@injectable()
export class FsService {

    constructor(
        @inject(DIConfigService) private readonly config: ConfigService,
    ) {
    }

    async readLastSnapshot(): Promise<Buffer> {
        return await this.readFile(path.join(this.config.storePath, this.config.snapshotOptions.filename));
    }

    async readFile(lastFilePath: string): Promise<Buffer> {
        return new Promise((res, rej) => {
            fs.readFile(lastFilePath, (err, contents) => {
                if (err) {
                    return rej(err);
                }
                return res(contents);
            });
        });
    }

}
