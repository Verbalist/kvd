import { Provider } from 'packages/inversify-modules/src/interfaces';
import { DISnapshotService, ISnapshotService, SnapshotService } from 'src/store/service/snapshot.service';
import {
    DISnapshotSerialize,
    ISnapshotSerialize,
    SnapshotSerialize,
} from 'src/store/service/serialize/snapshot.serialize';
import { DILogService, ILogService, LogService } from 'src/store/service/log.service';
import { DILogSerialize, ILogSerialize, LogSerialize } from 'src/store/service/serialize/log.serialize';
import { DIFsService, FsService } from 'src/store/service/fs.service';

export const snapshotServiceProvider: Provider = {
    provide: DISnapshotService,
    useContainer: container =>
        container.bind<ISnapshotService>(DISnapshotService).to(SnapshotService),
};

export const snapshotSerializeProvider: Provider = {
    provide: DISnapshotSerialize,
    useContainer: container =>
        container.bind<ISnapshotSerialize>(DISnapshotSerialize).to(SnapshotSerialize),
};

export const logServiceProvider: Provider = {
    provide: DILogService,
    useContainer: container =>
        container.bind<ILogService>(DILogService).to(LogService),
};

export const logSerializeProvider: Provider = {
    provide: DILogSerialize,
    useContainer: container =>
        container.bind<ILogSerialize>(DILogSerialize).to(LogSerialize),
};

export const fsServiceProvider: Provider = {
    provide: DIFsService,
    useContainer: container => container.bind<FsService>(DIFsService).to(FsService),
};
