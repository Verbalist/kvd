import { module } from 'inversify-modules';
import { ConfigModule } from 'src/config/config.module';
import { DISnapshotService } from 'src/store/service/snapshot.service';
import {
    fsServiceProvider,
    logSerializeProvider,
    logServiceProvider,
    snapshotSerializeProvider,
    snapshotServiceProvider,
} from 'src/store/store.providers';
import { configProvider, configServiceProvider } from 'src/config/config.provider';
import { DILogService } from 'src/store/service/log.service';

@module({
    imports: [ConfigModule],
    exports: [DISnapshotService, DILogService],
    providers: [
        snapshotServiceProvider,
        snapshotSerializeProvider,
        logServiceProvider,
        logSerializeProvider,
        configServiceProvider,
        configProvider,
        fsServiceProvider,
    ],
})
export class StoreModule {

}
