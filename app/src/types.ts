export type SupportedTypes = string | number;
export type RowHashArray = [string, SupportedTypes][];
export type RowHashArrayWithType = [number, string, SupportedTypes][];
export type Store = Map<string, SupportedTypes>;
export type Snapshot = {
    height: number,
    storage: Store,
};


export enum CsvValueType {
    number,
    string,
}

export enum ModeType {
    master = 'master',
    slave = 'slave',
}
