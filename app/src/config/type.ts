import { RabbitMQConfigSchema } from 'src/transport/rabbitmq/type';
import { AddressSchema } from 'src/shared/type/address';
import { SyncOptionsSchema } from 'src/shared/type/syncOptions';
import { SnapshotOptionsSchema } from 'src/shared/type/snapshotOptions';
import { ModeType } from 'src/types';

export const DIConfig = Symbol.for('Config');

export type ConfigSchema = {
    mode: ModeType,
    port: number,
    storePath: string,
    rabbitMQ: RabbitMQConfigSchema,
    master: AddressSchema,
    syncOptions: SyncOptionsSchema,
    snapshotOptions: SnapshotOptionsSchema
};
