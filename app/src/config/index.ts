import { ConfigSchema } from 'src/config/type';
import { ModeType } from 'src/types';
import { resolve } from 'path';

export const DEFAULT_SNAPSHOT_NAME = 'snapshot.csv';
export const DEFAULT_STORE_FOLDER = '/data/kvdstore/';

export const config: ConfigSchema = {
    mode: String(process.env.MODE) as ModeType,
    port: Number(process.env.DB_PORT),
    storePath: resolve(__dirname, `${process.env.STORE_FOLDER || `../..${DEFAULT_STORE_FOLDER}`}`),
    rabbitMQ: {
        host: String(process.env.RABBITMQ_HOST),
        port: Number(process.env.RABBITMQ_PORT),
        incomeQueue: String(process.env.RABBITMQ_INCOME_QUEUE),
        outcomeQueue: String(process.env.RABBITMQ_OUTCOME_QUEUE),
    },
    master: {
        host: String(process.env.MASTER_HOST),
        port: Number(process.env.MASTER_PORT),
        protocol: String(process.env.MASTER_PROTOCOL),
    },
    syncOptions: {
        timerInterval: Number(process.env.SYNCHRONIZATION_TIMER_INTERVAL),
        batchLimit: Number(process.env.SYNCHRONIZATION_BATCH_LIMIT),
    },
    snapshotOptions: {
        timerInterval: Number(process.env.SNAPSHOT_INTERVAL),
        filename: String(process.env.SNAPSHOT_NAME || DEFAULT_SNAPSHOT_NAME),
    },
};
