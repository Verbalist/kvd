import { inject, injectable } from 'inversify';
import { RabbitMQConfigSchema } from 'src/transport/rabbitmq/type';
import { ConfigSchema, DIConfig } from 'src/config/type';
import { AddressSchema } from 'src/shared/type/address';
import { SyncOptionsSchema } from 'src/shared/type/syncOptions';
import { SnapshotOptionsSchema } from 'src/shared/type/snapshotOptions';
import { ModeType } from 'src/types';

export const DIConfigService = Symbol.for('ConfigService');

@injectable()
export class ConfigService {
    readonly mode: ModeType;
    readonly port: number;
    readonly storePath: string;
    readonly rabbitMQ: RabbitMQConfigSchema;
    readonly master: AddressSchema;
    readonly syncOptions: SyncOptionsSchema;
    readonly snapshotOptions: SnapshotOptionsSchema;

    constructor(
        @inject(DIConfig) config: ConfigSchema,
    ) {
        this.mode = config.mode;
        this.port = config.port;
        this.storePath = config.storePath;
        this.rabbitMQ = config.rabbitMQ;
        this.master = config.master;
        this.syncOptions = config.syncOptions;
        this.snapshotOptions = config.snapshotOptions;
    }

    isMasterMode(): boolean {
        return this.mode === ModeType.master;
    }
}
