import { module } from 'inversify-modules';
import { DIConfigService } from 'src/config/config.service';
import { configProvider, configServiceProvider } from 'src/config/config.provider';

@module({
    imports: [],
    exports: [
        DIConfigService,
    ],
    providers: [
        configProvider,
        configServiceProvider,
    ],
})
export class ConfigModule {

}
