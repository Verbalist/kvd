import { Provider } from 'inversify-modules/src/interfaces';
import { ConfigSchema, DIConfig } from 'src/config/type';
import { config } from 'src/config';
import { ConfigService, DIConfigService } from 'src/config/config.service';

export const configProvider: Provider = {
    provide: DIConfig,
    useContainer: container =>
        container.bind<ConfigSchema>(DIConfig).toConstantValue(config),
};

export const configServiceProvider: Provider = {
    provide: DIConfigService,
    useContainer: container =>
        container.bind<ConfigService>(DIConfigService).to(ConfigService),
};
