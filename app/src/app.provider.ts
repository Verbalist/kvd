import { Provider } from 'inversify-modules/src/interfaces';
import { DIAppService, AppService } from 'src/app.service';

export const appServiceProvider: Provider = {
    provide: DIAppService,
    useContainer: container =>
        container.bind<AppService>(DIAppService).to(AppService),
};
