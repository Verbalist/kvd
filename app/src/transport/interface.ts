export interface ITransport {
    init(): Promise<void>;
    close(): Promise<void>;
}
