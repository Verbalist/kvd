import { injectable, inject } from 'inversify';
import { DICoreService, CoreService } from 'src/core/core.service';
import { Query, QueryValue, QueryKey, QueryType, QueryResponseSchema, QueryPair, QueryError } from 'src/transport/query/query';
import { ResponseEntity } from 'src/shared/model/response';
import { DIConfigService, ConfigService } from 'src/config/config.service';

export const DIQueryService = Symbol.for('QueryService');

@injectable()
export class QueryService {
    constructor(
        @inject(DICoreService) private coreService: CoreService,
        @inject(DIConfigService) private configService: ConfigService,
    ) { }

    private processSingleType(
        type: QueryType,
        key: QueryKey,
        value?: QueryValue,
    ): any {
        switch (type) {
            case QueryType.get:
                return this.coreService.get(key);
            case QueryType.set:
                this.coreService.set(key, value!);
                return true;
            case QueryType.delete:
                return this.coreService.delete(key);
            default:
                break;
        }
    }

    private processMultipleType(query: Query): any {
        const queryPairs = query.getPairs();
        let result: any;

        switch (query.type) {
            case QueryType.mget:
                result = this.coreService.mget(queryPairs.map(([key]) => key));
                break;
            case QueryType.mset:
                this.coreService.mset(queryPairs.map(([key, value]) => [key, value!]));
                result = true;
                break;
            case QueryType.mdelete:
                result = this.coreService.mdelete(queryPairs.map(([key]) => key));
                break;
            default:
                break;
        }

        return result;
    }

    process(query: Query, force: boolean = false): ResponseEntity<QueryResponseSchema> {
        let result: any;

        if (query.isModification() && !this.configService.isMasterMode() && !force) {
            return new ResponseEntity({
                errors: [QueryError.MODIFICATION_IS_DISABLED],
                data: {
                    type: query.type,
                    value: query.value,
                },
            });
        }

        if (query.isMultipleType()) {
            result = this.processMultipleType(query);
        } else {
            const queryPairs = query.getPairs();
            result = this.processSingleType(query.type, queryPairs[0][0], queryPairs[0][1]);
        }

        return new ResponseEntity({
            data: {
                type: query.type,
                value: query.value,
                result,
            },
        });
    }
}
