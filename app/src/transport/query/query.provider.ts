import { Provider } from 'inversify-modules/src/interfaces';
import { DIQueryService, QueryService } from 'src/transport/query/query.service';

export const queryServiceProvider: Provider = {
    provide: DIQueryService,
    useContainer: container =>
        container.bind<QueryService>(DIQueryService).to(QueryService),
};
