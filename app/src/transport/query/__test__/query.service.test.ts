import { createTestContainer } from 'src/__test__/container';
import { Query, QueryType } from 'src/transport/query/query';
import { QueryService, DIQueryService } from 'src/transport/query/query.service';
import { CoreService, DICoreService } from 'src/core/core.service';
import { deleteFile } from 'src/store/__test__/util';
import { ConfigService, DIConfigService } from 'src/config/config.service';

describe('Query Service', () => {
    let queryeService: QueryService;
    let coreService: CoreService;
    let configService: ConfigService;

    beforeEach(async () => {
        const container = createTestContainer();

        queryeService = container.get<QueryService>(DIQueryService);
        coreService = container.get<CoreService>(DICoreService);
        configService = container.get<ConfigService>(DIConfigService);
    });

    it('get nonexistent key', () => {
        const query = new Query({
            type: QueryType.get,
            value: 'k1',
        });
        const response = queryeService.process(query);

        expect(response.success).toEqual(false);
    });

    it('set key', () => {
        const query = new Query({
            type: QueryType.set,
            value: ['k2', 'v2'],
        });
        const response = queryeService.process(query);

        expect(response.success).toEqual(true);
        expect(coreService.get('k2')).toEqual('v2');
    });

    it('delete exists key', () => {
        const setQuery = new Query({
            type: QueryType.set,
            value: ['k3', 'v3'],
        });
        queryeService.process(setQuery);

        const deleteQuery = new Query({
            type: QueryType.delete,
            value: 'k3',
        });
        const deleteResponse = queryeService.process(deleteQuery);

        expect(deleteResponse.success).toEqual(true);
        expect(coreService.get('k3')).toEqual(undefined);
    });

    it('delete nonexistent key', () => {
        const deleteQuery = new Query({
            type: QueryType.delete,
            value: 'k4',
        });
        const deleteResponse = queryeService.process(deleteQuery);

        expect(deleteResponse.success).toEqual(false);
    });

    it('set multiple keys', () => {
        const query = new Query({
            type: QueryType.mset,
            value: [['k5', 'v5'], ['k6', 'v6']],
        });
        const response = queryeService.process(query);

        expect(response.success).toEqual(true);
        expect(coreService.get('k5')).toEqual('v5');
        expect(coreService.get('k6')).toEqual('v6');
    });

    it('delete multiple keys', () => {
        const setQuery = new Query({
            type: QueryType.mset,
            value: [['k7', 'v7'], ['k8', 'v8']],
        });
        queryeService.process(setQuery);

        const deleteQuery = new Query({
            type: QueryType.mdelete,
            value: ['k7', 'k8'],
        });
        const deleteResponse = queryeService.process(deleteQuery);

        expect(deleteResponse.success).toEqual(true);
        expect(coreService.get('k7')).toEqual(undefined);
        expect(coreService.get('k8')).toEqual(undefined);
    });

    it('get exists key', () => {
        const setQuery = new Query({
            type: QueryType.set,
            value: ['k9', 'v9'],
        });
        queryeService.process(setQuery);

        const query = new Query({
            type: QueryType.get,
            value: 'k9',
        });
        const response = queryeService.process(query);

        expect(response.success).toEqual(true);
    });

    afterAll(async () => {
        await deleteFile(`${configService.storePath}/log.csv`);
    });


});
