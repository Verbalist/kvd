import { Query, QueryType } from 'src/transport/query/query';

describe('Query Service', () => {
    it('get pairs from single key', () => {
        const query = new Query({
            type: QueryType.get,
            value: 'k1',
        });

        const pairs = query.getPairs();
        const expectedPairs = [['k1']];

        expect(pairs).toEqual(expectedPairs);
    });

    it('get pairs from multiple keys', () => {
        const query = new Query({
            type: QueryType.mget,
            value: ['k1', 'k2'],
        });

        const pairs = query.getPairs();
        const expectedPairs = [['k1'], ['k2']];

        expect(pairs).toEqual(expectedPairs);
    });

    it('get pairs from pair', () => {
        const query = new Query({
            type: QueryType.set,
            value: ['k1', 'v1'],
        });

        const pairs = query.getPairs();
        const expectedPairs = [['k1', 'v1']];

        expect(pairs).toEqual(expectedPairs);
    });

    it('get pairs from multiple pairs', () => {
        const query = new Query({
            type: QueryType.mset,
            value: [['k1', 'v1'], ['k2', 'v2']],
        });

        const pairs = query.getPairs();
        const expectedPairs = [['k1', 'v1'], ['k2', 'v2']];

        expect(pairs).toEqual(expectedPairs);
    });

    describe('Is valid positive cases', () => {
        it('get type with key', () => {
            const query = new Query({
                type: QueryType.get,
                value: 'k1',
            });

            expect(query.isValid()).toEqual(true);
        });

        it('set type with pair (value is string)', () => {
            const query = new Query({
                type: QueryType.set,
                value: ['k1', 'v1'],
            });

            expect(query.isValid()).toEqual(true);
        });

        it('set type with pair (value is number)', () => {
            const query = new Query({
                type: QueryType.set,
                value: ['k1', 1],
            });

            expect(query.isValid()).toEqual(true);
        });

        it('mget type with array of key', () => {
            const query = new Query({
                type: QueryType.mget,
                value: ['k1', 'k2', 'k3'],
            });

            expect(query.isValid()).toEqual(true);
        });

        it('mset type with array of pair', () => {
            const queries = [
                new Query({
                    type: QueryType.mset,
                    value: [['k1', 'v1'], ['k2', 'v2']],
                }),
                new Query({
                    type: QueryType.mset,
                    value: [['k3', 1], ['k4', 1]],
                }),
            ];

            expect(queries.every(query => query.isValid())).toEqual(true);
        });
    });
});
