import { module } from 'inversify-modules';
import { CoreModule } from 'src/core/core.module';
import { DIQueryService } from 'src/transport/query/query.service';
import { queryServiceProvider } from 'src/transport/query/query.provider';
import { ConfigModule } from 'src/config/config.module';

@module({
    imports: [CoreModule, ConfigModule],
    exports: [DIQueryService],
    providers: [queryServiceProvider],
})
export class QueryModule { }
