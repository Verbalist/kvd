import { SupportedTypes } from 'src/types';

export enum QueryType {
    get = 'get',
    set = 'set',
    delete = 'delete',
    mget = 'mget',
    mset = 'mset',
    mdelete = 'mdelete',
}

export type QueryKey = string;
export type QueryValue = SupportedTypes;
export type QueryPair = [QueryKey, QueryValue];

export type QueryResponseSchema<T = any> = {
    type: QueryType,
    value: QueryKey | QueryPair | Array<QueryKey> | Array<QueryPair>,
    result?: T,
};

export enum QueryError {
    CANNOT_PROCESS = 'Unable to process the query',
    MODIFICATION_IS_DISABLED = 'Modification is disabled in slave mode',
    INVALID_QUERY = 'Invalid query',
}

export type QuerySchema = {
    type: QueryType,
    value: QueryKey | Array<QueryKey> | QueryPair | Array<QueryPair>,
};

const QueryPairLength = 2;

/* Value examples
- Single type
-- 'k1'
-- ['k1', 'v1']

- Multiple type
-- ['k1', 'k2']
-- [[k1, v1], [k2, v2]]
*/

export class Query implements QuerySchema {
    readonly type: QueryType;
    readonly value: QueryKey | Array<QueryKey> | QueryPair | Array<QueryPair>;

    constructor(schema: QuerySchema) {
        this.type = schema.type;
        this.value = schema.value;
    }

    isModification(): boolean {
        if (
            this.type === QueryType.set ||
            this.type === QueryType.delete ||
            this.type === QueryType.mset ||
            this.type === QueryType.mdelete
        ) {
            return true;
        }

        return false;
    }

    isMultipleType(): boolean {
        if (this.type === QueryType.mdelete ||
            this.type === QueryType.mget ||
            this.type === QueryType.mset
        ) {
            return true;
        }

        return false;
    }

    private isKey(element: QueryKey): boolean {
        if (typeof element === 'string') {
            return true;
        }

        return false;
    }

    private isValue(element: QueryValue): boolean {
        if (typeof element === 'string' || typeof element === 'number') {
            return true;
        }

        return false;
    }

    private isPair = (element: QueryPair): boolean => {
        if (element.length !== QueryPairLength) {
            return false;
        }

        const [key, value] = element;

        if (!this.isKey(key) || !this.isValue(value)) {
            return false;
        }

        return true;
    };

    isValid(): boolean {
        switch (this.type) {
            case QueryType.get:
            case QueryType.delete:
                if (!this.isKey(this.value as QueryKey)) {
                    return false;
                }
                break;
            case QueryType.set:
                if (!this.isPair(this.value as QueryPair)) {
                    return false;
                }
                break;
            case QueryType.mget:
            case QueryType.mdelete:
                if (!Array.isArray(this.value)) {
                    return false;
                }
                if (!(this.value as Array<QueryKey>).every(this.isKey)) {
                    return false;
                }
                break;
            case QueryType.mset:
                if (!Array.isArray(this.value)) {
                    return false;
                }
                if (!(this.value as Array<QueryPair>).every(this.isPair)) {
                    return false;
                }
                break;
            default:
                return false;
        }

        return true;
    }

    getPairs(): Array<[string, QueryValue?]> {
        const pairs: Array<[string, QueryValue?]> = [];
        if (this.isMultipleType()) {
            if (Array.isArray(this.value) && typeof this.value[0] === 'string') {
                (this.value as Array<QueryKey>).forEach(key => pairs.push([key]));
            } else if (Array.isArray(this.value) && Array.isArray(this.value[0])) {
                (this.value as Array<QueryPair>).forEach((pair: QueryPair) => pairs.push(pair));
            }
        } else {
            if (typeof this.value === 'string') {
                pairs.push([this.value as QueryKey]);
            } else if (Array.isArray(this.value)) {
                pairs.push([this.value[0] as QueryKey, this.value[1] as QueryValue]);
            }
        }

        return pairs;
    }
}
