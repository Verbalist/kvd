import { Provider } from 'inversify-modules/src/interfaces';
import { DIHTTPService, HTTPService } from 'src/transport/http/http.service';

export const HTTPServiceProvider: Provider = {
    provide: DIHTTPService,
    useContainer: container =>
        container.bind<HTTPService>(DIHTTPService).to(HTTPService),
};
