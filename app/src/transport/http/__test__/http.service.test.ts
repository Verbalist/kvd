const waitPort = require('wait-port');
import fetch from 'node-fetch';
import { createTestContainer } from 'src/__test__/container';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { QuerySchema, QueryType, QueryResponseSchema } from 'src/transport/query/query';
import { HTTPService, DIHTTPService } from 'src/transport/http/http.service';
import { ResponseEntity } from 'src/shared/model/response';

describe('HTTP service', () => {
    let httpService: HTTPService;
    let configService: ConfigService;

    beforeEach(async () => {
        const container = createTestContainer();

        httpService = container.get<HTTPService>(DIHTTPService);
        configService = container.get<ConfigService>(DIConfigService);

        await httpService.init();
    });

    afterEach(async () => {
        await httpService.close();
    });

    it('processes queries', async () => {
        const host = '127.0.0.1';
        const { port } = configService;

        console.log(`Wait for http server...`);
        await waitPort({ host, port });

        const testQuery: QuerySchema = {
            type: QueryType.set,
            value: ['a', 'b'],
        };

        const expectedResponse = new ResponseEntity<QueryResponseSchema>({
            data: {
                type: QueryType.set,
                value: ['a', 'b'],
                result: true,
            },
        });

        const json = await fetch(`http://${host}:${port}/api/v1/query`, {
            method: 'post',
            body: JSON.stringify(testQuery),
            headers: { 'Content-Type': 'application/json' },
        }).then(res => res.json());

        expect(json).toEqual(expectedResponse);
    });
});
