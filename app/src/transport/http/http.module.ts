import { module } from 'inversify-modules';
import { ConfigModule } from 'src/config/config.module';
import { DIHTTPService } from 'src/transport/http/http.service';
import { HTTPServiceProvider } from 'src/transport/http/http.provider';
import { QueryModule } from 'src/transport/query/query.module';
import { StoreModule } from 'src/store/store.module';

@module({
    imports: [ConfigModule, QueryModule, StoreModule],
    exports: [DIHTTPService],
    providers: [HTTPServiceProvider],
})
export class HTTPModule { }
