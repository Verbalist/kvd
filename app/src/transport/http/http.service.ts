import { injectable, inject } from 'inversify';
import * as http from 'http';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import fetch from 'node-fetch';
import { Query } from 'src/transport/query/query';
import { HTTPStatus } from 'src/transport/http/type';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { DIQueryService, QueryService } from 'src/transport/query/query.service';
import { ITransport } from 'src/transport/interface';
import { ResponseEntity } from 'src/shared/model/response';
import { AddressSchema } from 'src/shared/type/address';
import { QueryBatchSchema } from 'src/shared/type/queryBatch';
import { DILogService, LogService } from 'src/store/service/log.service';

export const GET_BATCH_ROUTE = '/api/v1/query-batch';

export const DIHTTPService = Symbol.for('HTTPService');

@injectable()
export class HTTPService implements ITransport {
    private readonly port: number;
    private server: http.Server | undefined;

    constructor(
        @inject(DIConfigService) private configService: ConfigService,
        @inject(DIQueryService) private queryService: QueryService,
        @inject(DILogService) private logService: LogService,
    ) {
        this.port = this.configService.port;
    }

    async init(): Promise<void> {
        const app = express();

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(cors());

        app.post('/api/v1/query', this.query);

        app.post(GET_BATCH_ROUTE, this.queryBatch);

        return new Promise((resolve) => {
            this.server = app.listen(this.port, () => {
                console.log(`[HTTP] Listening on port ${this.port}!`);
                resolve();
            });
        });
    }

    private query = (req: express.Request, res: express.Response): express.Response => {
        const query = new Query(req.body);
        if (!query.isValid()) {
            return res.sendStatus(HTTPStatus.BAD_REQUEST);
        }

        const result = this.queryService.process(query);

        return res.send(result);
    };

    queryBatch = async (req: express.Request, res: express.Response): Promise<express.Response> => {
        const data: QueryBatchSchema = req.body;
        const result = await this.logService.readBatch(data.offset, data.limit);
        return res.send(result);
    };

    async fetch<T, D>(address: AddressSchema, path: string, data: D): Promise<ResponseEntity<T>> {
        const body = JSON.stringify(data);
        const response = await fetch(`${address.protocol}://${address.host}:${address.port}${path}`, {
            method: 'POST',
            body,
            headers: { 'Content-Type': 'application/json' },
        });
        if (response.ok) {
            const result = await response.json();
            return new ResponseEntity<T>({ data: result });
        } else {
            return new ResponseEntity<T>({ errors: [`${response.status}: ${response.statusText}`] });
        }
    }

    async close(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.server) {
                return reject('Server is not running!');
            }

            this.server.close(err => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve();
            });
        });
    }
}
