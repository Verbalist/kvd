const waitPort = require('wait-port');
import { createTestContainer } from 'src/__test__/container';
import { RabbitMQService, DIRabbitMQService } from 'src/transport/rabbitmq/rabbitmq.service';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { connect } from 'amqplib';
import { QuerySchema, QueryType, QueryResponseSchema } from 'src/transport/query/query';
import { ResponseEntity } from 'src/shared/model/response';

describe('RabbitMQ service', () => {
    let rabbitMQService: RabbitMQService;
    let configService: ConfigService;

    beforeEach(async () => {
        const container = createTestContainer();

        rabbitMQService = container.get<RabbitMQService>(DIRabbitMQService);
        configService = container.get<ConfigService>(DIConfigService);

        await rabbitMQService.init();
    });

    afterEach(async () => {
        await rabbitMQService.close();
    });

    it('processes queries', async () => {
        const { host, port, incomeQueue, outcomeQueue } = configService.rabbitMQ;

        console.log(`Wait for rabbit mq...`);
        await waitPort({ host, port });

        const connection = await connect(`amqp://${host}:${port}`);

        const incomeChannel = await connection.createChannel();
        await incomeChannel.assertQueue(incomeQueue);

        const testQuery: QuerySchema = {
            type: QueryType.set,
            value: ['a', 'b'],
        };
        incomeChannel.sendToQueue(incomeQueue, Buffer.from(JSON.stringify(testQuery)));

        const outcomeChannel = await connection.createChannel();
        await outcomeChannel.assertQueue(outcomeQueue);

        outcomeChannel.consume(outcomeQueue, async (serializedResponse) => {
            if (!serializedResponse) {
                return;
            }

            outcomeChannel.ack(serializedResponse);

            const response = new ResponseEntity<QueryResponseSchema>(
                JSON.parse(serializedResponse.content.toString()),
            );
            if (response.data!.type === testQuery.type) {
                expect(response.success).toEqual(true);

                await connection.close();
            }
        });
    });
});
