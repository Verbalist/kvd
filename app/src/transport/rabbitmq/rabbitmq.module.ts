import { module } from 'inversify-modules';
import { DIRabbitMQService } from 'src/transport/rabbitmq/rabbitmq.service';
import { ConfigModule } from 'src/config/config.module';
import { rabbitMQServiceProvider } from 'src/transport/rabbitmq/rabbitmq.provider';
import { QueryModule } from 'src/transport/query/query.module';

@module({
    imports: [ConfigModule, QueryModule],
    exports: [DIRabbitMQService],
    providers: [rabbitMQServiceProvider],
})
export class RabbitMQModule { }
