const waitPort = require('wait-port');

import { connect, Connection } from 'amqplib';
import { injectable, inject } from 'inversify';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { ModeType } from 'src/types';
import { Query, QueryResponseSchema, QueryError } from 'src/transport/query/query';
import { DIQueryService, QueryService } from 'src/transport/query/query.service';
import { ITransport } from 'src/transport/interface';
import { ResponseEntity } from 'src/shared/model/response';

export const DIRabbitMQService = Symbol.for('RabbitMQService');

@injectable()
export class RabbitMQService implements ITransport {
    private readonly host: string;
    private readonly port: number;
    private readonly incomeQueue: string;
    private readonly outcomeQueue: string;

    private connection: Connection | undefined;

    constructor(
        @inject(DIConfigService) private configService: ConfigService,
        @inject(DIQueryService) private queryService: QueryService,
    ) {
        this.host = this.configService.rabbitMQ.host;
        this.port = this.configService.rabbitMQ.port;
        this.incomeQueue = this.configService.rabbitMQ.incomeQueue;
        this.outcomeQueue = this.configService.rabbitMQ.outcomeQueue;
    }

    async init(): Promise<void> {
        if (this.configService.mode !== ModeType.master) {
            return;
        }

        console.log(`[RabbitMQ] Wait for rabbit mq...`);
        await waitPort({ host: this.host, port: this.port });

        this.connection = await connect(`amqp://${this.host}:${this.port}`);

        const incomeChannel = await this.connection.createChannel();
        await incomeChannel.assertQueue(this.incomeQueue);

        const outcomeChannel = await this.connection.createChannel();
        await outcomeChannel.assertQueue(this.outcomeQueue);

        await incomeChannel.consume(this.incomeQueue, (serializedQuery) => {
            if (!serializedQuery) {
                return;
            }

            const parsedQuery = JSON.parse(serializedQuery.content.toString());
            const query = new Query(parsedQuery);

            incomeChannel.ack(serializedQuery);

            if (!query.isValid()) {
                console.log('[amqp][income] invalid query', parsedQuery);

                const errorResponse = new ResponseEntity<QueryResponseSchema>({
                    data: {
                        type: query.type,
                        value: query.value,
                    },
                    errors: [QueryError.INVALID_QUERY],
                });

                outcomeChannel.sendToQueue(this.outcomeQueue, Buffer.from(JSON.stringify(errorResponse)));

                return;
            }

            console.log('[amqp][income]', parsedQuery);

            const response = this.queryService.process(query);

            outcomeChannel.sendToQueue(this.outcomeQueue, Buffer.from(JSON.stringify(response)));
        });

        console.log('[Service][RabbitMQ] Initialized');
    }

    async close() {
        if (!this.connection) {
            return;
        }

        await this.connection.close();
    }
}
