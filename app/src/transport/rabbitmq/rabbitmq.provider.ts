import { Provider } from 'inversify-modules/src/interfaces';
import { DIRabbitMQService, RabbitMQService } from 'src/transport/rabbitmq/rabbitmq.service';

export const rabbitMQServiceProvider: Provider = {
    provide: DIRabbitMQService,
    useContainer: container =>
        container.bind<RabbitMQService>(DIRabbitMQService).to(RabbitMQService),
};
