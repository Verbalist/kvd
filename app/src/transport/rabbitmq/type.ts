export type RabbitMQConfigSchema = {
    host: string,
    port: number,
    incomeQueue: string,
    outcomeQueue: string,
};
