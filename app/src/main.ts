import 'reflect-metadata';
import { bootstrapModule } from 'packages/inversify-modules/src';
import { AppModule } from 'src/app.module';

const app = bootstrapModule(AppModule);
app.init();

process.on('beforeExit', async () => {
    await app.close();
});
