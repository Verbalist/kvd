import { inject, injectable } from 'inversify';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { CoreService, DICoreService } from 'src/core/core.service';
import { DIRabbitMQService } from 'src/transport/rabbitmq/rabbitmq.service';
import { DIHTTPService } from 'src/transport/http/http.service';
import { ITransport } from 'src/transport/interface';
import { DIReplicationService, ReplicationService } from 'src/replication/replication.service';
import { DISchedulerService, SchedulerService } from 'src/scheduler/scheduler.service';
import { DIStreamService, StreamService } from 'src/stream/stream.service';
import { ModeType } from 'src/types';

export const DIAppService = Symbol.for('AppService');

@injectable()
export class AppService {

    constructor(
        @inject(DIConfigService) private configService: ConfigService,
        @inject(DICoreService) private coreService: CoreService,
        @inject(DIRabbitMQService) private rabbitMQService: ITransport,
        @inject(DIHTTPService) private httpService: ITransport,
        @inject(DIStreamService) private streamService: StreamService,
        @inject(DIReplicationService) private replicationService: ReplicationService,
        @inject(DISchedulerService) private schedulerService: SchedulerService,
    ) {
    }

    async init(): Promise<boolean> {
        console.log(`AppModule init in mode: ${this.configService.mode}`);

        this.streamService.init();
        this.schedulerService.init();

        await this.coreService.init();

        await this.rabbitMQService.init();
        await this.httpService.init();

        if (this.configService.mode === ModeType.slave) {
            this.replicationService.init();
        }
        return true;
    }

    async close(): Promise<boolean> {
        await this.rabbitMQService.close();
        await this.httpService.close();

        // TODO check test capability
        // await this.coreService.snapshot();

        await this.streamService.close();
        await this.schedulerService.close();

        return true;
    }
}
