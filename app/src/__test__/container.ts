import { Container } from 'inversify';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { CoreRepository, DICoreRepository } from 'src/core/core.repository';
import { DIRabbitMQService, RabbitMQService } from 'src/transport/rabbitmq/rabbitmq.service';
import { DISnapshotService, SnapshotService } from 'src/store/service/snapshot.service';
import { CoreService, DICoreService } from 'src/core/core.service';
import { ConfigSchema, DIConfig } from 'src/config/type';
import { ModeType } from 'src/types';
import { AppService, DIAppService } from 'src/app.service';
import { DIHTTPService, HTTPService } from 'src/transport/http/http.service';
import { DIQueryService, QueryService } from 'src/transport/query/query.service';
import { DIReplicationService, ReplicationService } from 'src/replication/replication.service';
import { DISchedulerService, SchedulerService } from 'src/scheduler/scheduler.service';
import { DIStreamService, StreamService } from 'src/stream/stream.service';
import { DIWarmUpService, WarmUpService } from 'src/core/warmUp.service';
import { DILogService, LogService } from 'src/store/service/log.service';
import { DILogSerialize, ILogSerialize, LogSerialize } from 'src/store/service/serialize/log.serialize';
import {
    DISnapshotSerialize,
    ISnapshotSerialize,
    SnapshotSerialize
} from 'src/store/service/serialize/snapshot.serialize';
import { DIFsService, FsService } from 'src/store/service/fs.service';
import { DEFAULT_SNAPSHOT_NAME, DEFAULT_STORE_FOLDER } from 'src/config';
import { resolve } from 'path';

export type TestContainerArgs = {
    incomeQueue?: string;
    outcomeQueue?: string;
    mode?: ModeType;
    httpPort?: number;
    syncTimerInterval?: number;
};

/* tslint:disable:no-magic-numbers */
export const createTestContainer = (args: TestContainerArgs = {}): Container => {
    const container = new Container({ defaultScope: 'Singleton' });

    const config: ConfigSchema = {
        mode: args.mode || ModeType.master,
        port: args.httpPort || 5042,
        storePath: resolve(__dirname, `${process.env.STORE_FOLDER || `../..${DEFAULT_STORE_FOLDER}`}`),
        rabbitMQ: {
            host: 'localhost',
            port: 5672,
            incomeQueue: args.incomeQueue || 'INCOME_QUEUE',
            outcomeQueue: args.outcomeQueue || 'OUTCOME_QUEUE',
        },
        master: {
            host: 'localhost',
            port: 5042,
            protocol: 'http',
        },
        syncOptions: {
            timerInterval: args.syncTimerInterval || 15000,
            batchLimit: 3,
        },
        snapshotOptions: {
            timerInterval: 60000,
            filename: DEFAULT_SNAPSHOT_NAME,

        },
    };

    container
    .bind<ConfigSchema>(DIConfig)
    .toConstantValue(config);
    container.bind<ConfigService>(DIConfigService).to(ConfigService);
    container.bind<CoreRepository>(DICoreRepository).to(CoreRepository);
    container.bind<CoreService>(DICoreService).to(CoreService);
    container.bind<WarmUpService>(DIWarmUpService).to(WarmUpService);
    container.bind<RabbitMQService>(DIRabbitMQService).to(RabbitMQService);
    container.bind<HTTPService>(DIHTTPService).to(HTTPService);
    container.bind<QueryService>(DIQueryService).to(QueryService);
    container.bind<SnapshotService>(DISnapshotService).to(SnapshotService);
    container.bind<StreamService>(DIStreamService).to(StreamService);
    container.bind<ReplicationService>(DIReplicationService).to(ReplicationService);
    container.bind<SchedulerService>(DISchedulerService).to(SchedulerService);
    container.bind<LogService>(DILogService).to(LogService);
    container.bind<ILogSerialize>(DILogSerialize).to(LogSerialize);
    container.bind<ISnapshotSerialize>(DISnapshotSerialize).to(SnapshotSerialize);
    container.bind<AppService>(DIAppService).to(AppService);
    container.bind<FsService>(DIFsService).to(FsService);

    return container;
};
/* tslint:enable:no-magic-numbers */
