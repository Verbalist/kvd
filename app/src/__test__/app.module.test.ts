import { createTestContainer } from 'src/__test__/container';
import { AppService, DIAppService } from 'src/app.service';

describe('AppModule', () => {

    test('Init app module', async () => {
        const container = createTestContainer();

        const appService = container.get<AppService>(DIAppService);

        const result = await appService.init();

        expect(result).toBe(true);

        await appService.close();
    });

});
