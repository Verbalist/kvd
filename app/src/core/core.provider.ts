import { Provider } from 'inversify-modules/src/interfaces';
import { DICoreRepository, CoreRepository } from 'src/core/core.repository';
import { DICoreService, CoreService } from 'src/core/core.service';
import { DIWarmUpService, WarmUpService } from 'src/core/warmUp.service';

export const coreRepositoryProvider: Provider = {
    provide: DICoreRepository,
    useContainer: container =>
        container.bind<CoreRepository>(DICoreRepository).to(CoreRepository),
};

export const coreServiceProvider: Provider = {
    provide: DICoreService,
    useContainer: container =>
        container.bind<CoreService>(DICoreService).to(CoreService),
};

export const warmUpServiceProvider: Provider = {
    provide: DIWarmUpService,
    useContainer: container =>
        container.bind<WarmUpService>(DIWarmUpService).to(WarmUpService),
};
