import { inject, injectable } from 'inversify';
import { CoreRepository, DICoreRepository } from 'src/core/core.repository';
import { DILogService, LogService } from 'src/store/service/log.service';
import { DISnapshotService, SnapshotService } from 'src/store/service/snapshot.service';

export const DIWarmUpService = Symbol.for('WarmUpService');

@injectable()
export class WarmUpService {

    constructor(
        @inject(DICoreRepository) private coreRepository: CoreRepository,
        @inject(DILogService) private logService: LogService,
        @inject(DISnapshotService) private snapshotService: SnapshotService
    ) {
    }

    async warmUp(): Promise<void> {
        const snapshot = await this.snapshotService.readLast();
        this.coreRepository.setStore(snapshot!.storage);
        this.coreRepository.height = snapshot!.height;
        console.log('Warm up done');
        // TODO warmUP least from log
    }

}
