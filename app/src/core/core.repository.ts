import { injectable } from 'inversify';
import { Store, SupportedTypes } from 'src/types';

export const DICoreRepository = Symbol.for('CoreRepository');

@injectable()
export class CoreRepository {

    private store: Store;
    public height: number;

    constructor() {
        this.height = 0;
        this.store = new Map();
    }

    set(key: string, value: SupportedTypes) {
        this.store.set(key, value);
    }

    get(key: string): SupportedTypes | undefined {
        return this.store.get(key);
    }

    delete(key: string): boolean {
        return this.store.delete(key);
    }

    incrementHeight() {
        return this.height += 1;
    }

    getStore(): Store {
        return this.store;
    }

    setStore(store: Store): void {
        this.store = store;
    }

}
