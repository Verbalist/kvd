import { inject, injectable } from 'inversify';
import { CoreRepository, DICoreRepository } from 'src/core/core.repository';
import { SupportedTypes } from 'src/types';
import { DILogService, LogService } from 'src/store/service/log.service';
import { DISnapshotService, SnapshotService } from 'src/store/service/snapshot.service';
import { QueryPair, QueryType } from 'src/transport/query/query';
import { DIStreamService, StreamService } from 'src/stream/stream.service';
import { DIWarmUpService, WarmUpService } from 'src/core/warmUp.service';
import { EVENT } from 'src/stream/event';

export const DICoreService = Symbol.for('CoreService');

@injectable()
export class CoreService {

    constructor(
        @inject(DICoreRepository) private coreRepository: CoreRepository,
        @inject(DIStreamService) private streamService: StreamService,
        @inject(DILogService) private logService: LogService,
        @inject(DISnapshotService) private snapshotService: SnapshotService,
        @inject(DIWarmUpService) private warmUpService: WarmUpService,
    ) {
        this.snapshot = this.snapshot.bind(this);
    }

    async init(): Promise<void> {
        this.snapshotService.init();
        this.logService.init();
        await this.warmUpService.warmUp();

        this.streamService.on(EVENT.TIME_TO_SNAPSHOT, this.snapshot);
        this.streamService.emit<void>(EVENT.SCHEDULE_SNAPSHOT_TASK);
        console.log('CoreService init');
    }

    get height(): number {
        return this.coreRepository.height;
    }

    set(key: string, value: SupportedTypes): void {
        // TODO will be awaited in next version
        this.logService.log({ type: QueryType.set, value: [key, value] });
        this.coreRepository.incrementHeight();
        this.coreRepository.set(key, value);
    }

    get(key: string): SupportedTypes | undefined {
        return this.coreRepository.get(key);
    }

    delete(key: string): boolean {
        // TODO will be awaited in next version
        this.logService.log({ type: QueryType.delete, value: key });
        this.coreRepository.incrementHeight();
        return this.coreRepository.delete(key);
    }

    mset(data: Array<QueryPair>): void {
        this.logService.log({ type: QueryType.mset, value: data });
        this.coreRepository.incrementHeight();
        data.forEach(([key, value]) => this.set(key, value));
    }

    mget(data: Array<string>): Array<SupportedTypes | undefined> {
        return data.map((key) => this.get(key));
    }

    mdelete(data: Array<string>): Array<boolean> {
        this.logService.log({ type: QueryType.mdelete, value: data });
        this.coreRepository.incrementHeight();
        return data.map((key) => this.delete(key));
    }

    private async snapshot(): Promise<void> {
        await this.snapshotService.create(this.coreRepository.height, this.coreRepository.getStore());
        console.log('Snapshot created');
    }

}
