import { module } from 'inversify-modules';
import { DICoreService } from 'src/core/core.service';
import { coreRepositoryProvider, coreServiceProvider, warmUpServiceProvider } from 'src/core/core.provider';
import { StreamModule } from 'src/stream/stream.module';
import { StoreModule } from 'src/store/store.module';

@module({
    imports: [
        StreamModule,
        StoreModule
    ],
    exports: [
        DICoreService,
    ],
    providers: [
        coreServiceProvider,
        coreRepositoryProvider,
        warmUpServiceProvider
    ],
})
export class CoreModule {

}
