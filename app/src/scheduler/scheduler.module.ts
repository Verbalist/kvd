import { module } from 'inversify-modules';
import { DISchedulerService, SchedulerService } from 'src/scheduler/scheduler.service';
import { SchedulerServiceProvider } from 'src/scheduler/scheduler.provider';
import { ConfigModule } from 'src/config/config.module';
import { StreamModule } from 'src/stream/stream.module';

@module({
    imports: [
        StreamModule,
        ConfigModule,
    ],
    exports: [
        DISchedulerService,
    ],
    providers: [
        SchedulerServiceProvider,
    ],
})
export class SchedulerModule {
}
