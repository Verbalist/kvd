import { inject, injectable } from 'inversify';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { DIStreamService, StreamService } from 'src/stream/stream.service';
import { EVENT } from 'src/stream/event';

export const DISchedulerService = Symbol.for('SchedulerService');

@injectable()
export class SchedulerService {

    // TODO rewrite to register pattern
    private timerSyncId: NodeJS.Timeout | undefined;
    private timerSnapshotId: NodeJS.Timeout | undefined;

    constructor(
        @inject(DIStreamService) private streamService: StreamService,
        @inject(DIConfigService) private configService: ConfigService,
    ) {
    }

    init(): void {
        console.log('SchedulerService init');
        this.streamService.on(EVENT.SCHEDULE_SYNC_TASK, this.syncTask);
        this.streamService.on(EVENT.SCHEDULE_SNAPSHOT_TASK, this.snapshotTask);
    }

    syncTask = (): void => {
        this.timerSyncId = setTimeout(async () => {
            await this.streamService.emit<void>(EVENT.TIME_TO_SYNC);
        }, this.configService.syncOptions.timerInterval);
    };

    snapshotTask = (): void => {
        this.timerSnapshotId = setInterval(async () => {
            await this.streamService.emit<void>(EVENT.TIME_TO_SNAPSHOT);
        }, this.configService.snapshotOptions.timerInterval);
    };

    close() {
        if (this.timerSyncId) {
            clearInterval(this.timerSyncId);
        }

        if (this.timerSnapshotId) {
            clearInterval(this.timerSnapshotId);
        }

    }
}
