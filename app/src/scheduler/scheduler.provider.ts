import { Provider } from 'inversify-modules/src/interfaces';
import { DIReplicationService, ReplicationService } from 'src/replication/replication.service';
import { DISchedulerService, SchedulerService } from 'src/scheduler/scheduler.service';

export const SchedulerServiceProvider: Provider = {
    provide: DISchedulerService,
    useContainer: container =>
        container.bind<SchedulerService>(DISchedulerService).to(SchedulerService),
};
