import { module } from 'inversify-modules';
import { DIReplicationService } from 'src/replication/replication.service';
import { ReplicationServiceProvider } from 'src/replication/replication.provider';
import { HTTPModule } from 'src/transport/http/http.module';
import { QueryModule } from 'src/transport/query/query.module';
import { ConfigModule } from 'src/config/config.module';
import { StreamModule } from 'src/stream/stream.module';
import { CoreModule } from 'src/core/core.module';

@module({
    imports: [
        HTTPModule,
        ConfigModule,
        QueryModule,
        StreamModule,
        CoreModule,
    ],
    exports: [
        DIReplicationService,
    ],
    providers: [
        ReplicationServiceProvider,
    ],
})
export class ReplicationModule {
}
