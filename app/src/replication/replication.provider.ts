import { Provider } from 'inversify-modules/src/interfaces';
import { DIReplicationService, ReplicationService } from 'src/replication/replication.service';

export const ReplicationServiceProvider: Provider = {
    provide: DIReplicationService,
    useContainer: container =>
        container.bind<ReplicationService>(DIReplicationService).to(ReplicationService),
};
