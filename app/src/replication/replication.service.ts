import { inject, injectable } from 'inversify';
import { DIHTTPService, GET_BATCH_ROUTE, HTTPService } from 'src/transport/http/http.service';
import { ConfigService, DIConfigService } from 'src/config/config.service';
import { DIQueryService, QueryService } from 'src/transport/query/query.service';
import { ResponseEntity } from 'src/shared/model/response';
import { QueryBatchSchema } from 'src/shared/type/queryBatch';
import { Query, QuerySchema } from 'src/transport/query/query';
import { DIStreamService, StreamService } from 'src/stream/stream.service';
import { EVENT } from 'src/stream/event';
import { CoreService, DICoreService } from 'src/core/core.service';

export const DIReplicationService = Symbol.for('ReplicationService');

@injectable()
export class ReplicationService {

    constructor(
        @inject(DIHTTPService) private httpService: HTTPService,
        @inject(DIConfigService) private configService: ConfigService,
        @inject(DIQueryService) private queryService: QueryService,
        @inject(DIStreamService) private streamService: StreamService,
        @inject(DICoreService) private coreService: CoreService,
    ) {
    }

    init(): void {
        console.log('ReplicationService init');
        this.streamService.on(EVENT.TIME_TO_SYNC, this.sync);
        this.streamService.emit<void>(EVENT.SCHEDULE_SYNC_TASK);
    }

    private async getData(): Promise<ResponseEntity<QuerySchema[]>> {

        const offset: number = this.coreService.height;
        const query: QueryBatchSchema = {
            limit: this.configService.syncOptions.batchLimit,
            offset,
        };
        return this.httpService.fetch<QuerySchema[], QueryBatchSchema>(
            this.configService.master,
            GET_BATCH_ROUTE,
            query,
        );
    }

    private processData(data: QuerySchema[]): void {
        data.map((row: QuerySchema) => {
            const query: Query = new Query(row);
            this.queryService.process(query, true);
        });
    }

    private sync = async (): Promise<void> => {

        const result = await this.getData();
        if (!result.success || !result.data) {
            this.streamService.emit(EVENT.SCHEDULE_SYNC_TASK);
            return;
        }

        this.processData(result.data);

        if (result.data.length < this.configService.syncOptions.batchLimit) {
            this.streamService.emit(EVENT.SCHEDULE_SYNC_TASK);
            return;
        }
        setImmediate(this.sync);
    };
}
