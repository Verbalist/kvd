const path = require('path');
const webpackBar = require('webpackbar');
const nodeExternals = require('webpack-node-externals');
const nodemon = require('nodemon-webpack-plugin')
const Dotenv = require('dotenv-webpack')

const DIR = path.resolve(__dirname, '..');
const OUTPUT_DIR = path.resolve(DIR, 'dist');

const config = {
    entry: path.join(DIR, 'src', 'main.ts'),
    context: path.resolve(DIR, 'src'),
    resolve: {
        extensions: ['.ts', '.js', '.json'],
        alias: {
            src: path.resolve(DIR, 'src'),
            packages: path.resolve(DIR, 'packages'),
        }
    },
    externals: [nodeExternals({
        whitelist: ['inversify-modules']
    })],
    output: {
        filename: 'main.js',
        path: OUTPUT_DIR,
        publicPath: '/',
    },
    node: {
        __dirname: true,
        __filename: true
    },
    mode: 'development',
    target: 'node',
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
        ],
    },
    plugins: [
        new webpackBar({color: '#33b400'}),
        new nodemon({
            watch: path.resolve(OUTPUT_DIR),
            nodeArgs: ['--trace-warnings'],
        }),
        new Dotenv({
            path: path.resolve(DIR, '../env/.env'),
        })
    ]
};


module.exports = config;
