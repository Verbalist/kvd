import { interfaces } from 'inversify';
import { ClassModule } from './interfaces';
import { Module } from './Module';
import { Container } from 'inversify';

export class ModuleBuilder {
    private target: ClassModule;

    private modules: Map<ClassModule, Module>;

    constructor(target: ClassModule) {
        this.target = target;
        this.modules = new Map();
    }

    initModule(target: ClassModule): Module {
        const module = this.modules.get(target);
        if (module) {
            return module;
        }

        const singleton = new Module(target);

        for (const item of singleton.imports) {
            const importModule = this.initModule(item);
            singleton.addExport(importModule, importModule.target);

            for (const identifier of importModule.exports) {
                singleton.addExport(importModule, identifier);
            }
        }

        singleton.resolve();

        this.modules.set(target, singleton);

        return singleton;
    }

    build(): Module {
        return this.initModule(this.target);
    }
}
