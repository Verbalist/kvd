import { Container, interfaces } from 'inversify';
import { ModuleProps, ClassModule, Provider, ClassProvider, ContainerProvider, ValueProvider } from './interfaces';
import { MODULE_METADATA_KEY } from './annotation/module';

export class Module {
    private container: Container;
    readonly target: ClassModule;

    readonly imports: Array<ClassModule>;
    readonly exports: Array<interfaces.ServiceIdentifier<any>>;
    private providers: Array<Provider>;

    constructor(target: ClassModule) {
        this.target = target;
        const props: ModuleProps = Reflect.getMetadata(MODULE_METADATA_KEY, target);
        if (!props) {
            throw new Error(`Missing required @module annotation in: ${target.name}.`);
        }

        this.imports = props.imports || [];
        this.exports = props.exports || [];
        this.providers = props.providers || [];

        this.container = new Container({defaultScope: 'Singleton'});
    }

    addExport(importModule: Module, identifier: interfaces.ServiceIdentifier<any>) {
        this.setProvider({
            provide: identifier,
            useContainer: container => {
                const provider = importModule.getProvider(identifier);
                container.bind(identifier).toConstantValue(provider);
            },
        });
    }

    getProvider<T>(serviceIdentifier: interfaces.ServiceIdentifier<T>): T {
        return this.container.get<T>(serviceIdentifier);
    }

    setProvider(provider: Provider) {
        if (typeof provider === 'function') {
            provider = { provide: provider, useClass: provider };
        }

        const { provide } = provider;

        if (this.container.isBound(provide)) {
            return;
        }

        if ((provider as ClassProvider).useClass) {
            return this.container.bind(provide).to((provider as ClassProvider).useClass);
        }

        if ((provider as ContainerProvider).useContainer) {
            return (provider as ContainerProvider).useContainer(this.container);
        }

        if ((provider as ValueProvider).useValue) {
            return this.container.bind(provide).toConstantValue((provider as ValueProvider).useValue);
        }
    }

    resolve() {
        this.setProvider(this.target);
        for (const provider of this.providers) {
            this.setProvider(provider);
        }
    }

}
