import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { module, ModuleBuilder, bootstrapModule } from '..';

/* tslint:disable:max-classes-per-file */
describe('ModuleBuilder', () => {

    test('two providers', () => {
        @injectable()
        class A {
        }

        @injectable()
        class B {
            constructor(a: A) {
            }
        }

        @module({
            providers: [
                A,
                B,
            ],
        })
        class TestModule {
            run() {
            }
        }

        const builder = new ModuleBuilder(TestModule);
        const testModule = builder.build();

        expect(testModule.getProvider(A)).toBeInstanceOf(A);
        expect(testModule.getProvider(B)).toBeInstanceOf(B);
        expect(testModule.getProvider(TestModule)).toBeInstanceOf(TestModule);
    });

    test('three providers', () => {
        @injectable()
        class A {
        }

        @injectable()
        class B {
            constructor(a: A) {
            }
        }

        @injectable()
        class C {
            constructor(a: A, b: B) {
            }
        }

        @module({
            providers: [
                A,
                B,
                C,
            ],
        })
        class TestModule {
        }

        const builder = new ModuleBuilder(TestModule);
        const testModule = builder.build();

        expect(testModule.getProvider(A)).toBeInstanceOf(A);
        expect(testModule.getProvider(B)).toBeInstanceOf(B);
        expect(testModule.getProvider(C)).toBeInstanceOf(C);

    });

    test('import module', () => {

        @injectable()
        class A {
        }

        @injectable()
        class B {
            constructor(a: A) {
            }
        }

        @module({
            exports: [
                A,
            ],
            providers: [
                A,
            ],
        })
        class ImportModule {
        }

        @module({
            imports: [
                ImportModule,
            ],
            providers: [
                B,
            ],
        })
        class TestModule {
        }

        const builder = new ModuleBuilder(TestModule);
        const testModule = builder.build();

        expect(testModule.getProvider(B)).toBeInstanceOf(B);
    });

    test('useValue', () => {

        const DIConfig = Symbol('Config');

        type ConfigSchema = {
            version: number;
        };
        const Config: ConfigSchema = {
            version: 1,
        };

        @injectable()
        class Provider {
            constructor(
                @inject(DIConfig) private readonly config: ConfigSchema,
            ) {
            }

            getConf() {
                return this.config;
            }
        }

        @module({
            providers: [
                Provider,
                {
                    provide: DIConfig,
                    useValue: Config,
                },
            ],
        })
        class Module {
            constructor(
                private readonly provider: Provider,
                @inject(DIConfig) private readonly config: ConfigSchema,
            ) {
            }

            getConf() {
                return this.config;
            }

        }

        const mod = bootstrapModule(Module);

        const conf = mod.getConf();
        expect(conf).toStrictEqual(Config);
        expect(conf).toBe(Config); // same ref

    });

    test('multiple modules', () => {

        try {
            @module({})
            @module({})
            class Module {

            }
        } catch (e) {
            expect(e.message).toContain('Cannot apply @module decorator multiple times.');
        }

    });

});
