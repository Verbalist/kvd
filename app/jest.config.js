module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    roots: ['<rootDir>/src/', '<rootDir>/packages/'],
    moduleNameMapper: {
        '^src/(.*)$': '<rootDir>/src/$1',
        '^packages/(.*)$': '<rootDir>/packages/$1',
    },
    modulePaths: ['<rootDir>/src/'],
    setupFiles: [
        './node_modules/reflect-metadata'
    ]
};
