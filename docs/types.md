# Types

## Operation Type

| Value   | Description                          |
|---------|--------------------------------------|
| get     | Get value from database              |
| set     | Set value to database                |
| delete  | Delete value from database           |
| mget    | Get multiple values from database    |
| mset    | Set multiple values to database      |
| mdelete | Delete multiple values from database |

## DB Key

Supported type is: string

## DB Value

Supported types are: string, number

## DB Pair

Array of two elements [[db key](#db-key), [db value](#db-value)]
