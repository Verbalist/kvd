# Rabbit MQ

## Queries

Send incoming requests to the incoming queue: `INCOME_QUEUE`

Result will be sent to outcoming queue: `OUTCOME_QUEUE`

### Get

Get the value of key

| Parameter | Type                         | Description                   |
|-----------|------------------------------|-------------------------------|
| type      | [operation](#operation-type) | Operation type, must be `get` |
| value     | [db key](types.md#db-key)    | Key for getting value         |

### Set

Set key to hold the string value. If key already holds a value, it is overwritten.

| Parameter | Type                         | Description                   |
|-----------|------------------------------|-------------------------------|
| type      | [operation](#operation-type) | Operation type, must be `set` |
| value     | [db pair](types.md#db-pair)  | Key, value                    |

### Delete

Removes the specified key.

| Parameter | Type                         | Description                      |
|-----------|------------------------------|----------------------------------|
| type      | [operation](#operation-type) | Operation type, must be `delete` |
| value     | [db key](types.md#db-key)    | Key for delete                   |

### Multi Get

Returns the values of all specified keys.

| Parameter | Type                             | Description                      |
|-----------|----------------------------------|----------------------------------|
| type      | [operation](#operation-type)     | Operation type, must be `mget`   |
| value     | Array<[db key](types.md#db-key)> | Array of keys for getting values |

### Multi Set

Sets the given keys to their respective values. MSET replaces existing values with new values, just as regular SET.

| Parameter | Type                               | Description                    |
|-----------|------------------------------------|--------------------------------|
| type      | [operation](#operation-type)       | Operation type, must be `mset` |
| value     | Array<[db pair](types.md#db-pair)> | Array of key, value            |

### Multi Delete

Removes the specified keys. A key is ignored if it does not exist.

| Parameter | Type                             | Description                       |
|-----------|----------------------------------|-----------------------------------|
| type      | [operation](#operation-type)     | Operation type, must be `mdelete` |
| value     | Array<[db key](types.md#db-key)> | Array of keys for delete          |

## Response

| Parameter | Type                            | Description                                     |
|-----------|---------------------------------|-------------------------------------------------|
| success   | boolean                         | Operation status                                |
| data      | [Response data](#response-data) | Response data                                   |
| errors?   | Array<[string]()>               | Array of errors. Is present if success is false |

### Response data

| Parameter | Type                         | Description          |
|-----------|------------------------------|----------------------|
| type      | [operation](#operation-type) | Query operation type |
| value     | Query type                   | Query value          |
| result    | Any                          | Query result         |
