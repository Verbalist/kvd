export const getErrors = (error: Error | string[]): string[] => {
    if (Array.isArray(error)) {
        return error;
    }
    return [error.message];
}
