import { interfaces } from 'inversify';
import { useContainer } from './useContainer';

export const useDI = <T>(serviceIdentifier: interfaces.ServiceIdentifier<T>): T => {
    return useContainer().get<T>(serviceIdentifier);
};
