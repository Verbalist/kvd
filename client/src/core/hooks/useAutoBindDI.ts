import React from 'react';
import { interfaces } from 'inversify';
import { useContainer } from './useContainer';

export const useAutoBindDI = <T>(serviceIdentifier: interfaces.ServiceIdentifier<T>): T => {
    const container = useContainer();
    return React.useMemo(() => {
        if (!container.isBound(serviceIdentifier)) {
            container.bind(serviceIdentifier).toSelf();
        }
        return container.get<T>(serviceIdentifier);
    }, [serviceIdentifier]);
};
