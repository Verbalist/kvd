import { interfaces } from 'inversify';
import { useAutoBindDI } from './useAutoBindDI';
import { MAKE_HOOK_DECORATOR_METADATA, MakeHookDecoratorMetadata } from '../decorator/makeHookDecorator';

export const useModel = <T, P = any>(serviceIdentifier: interfaces.ServiceIdentifier<T>, props?: P): T => {
    const model: any = useAutoBindDI(serviceIdentifier);

    const makeHookDecoratorMetadata: Map<string, MakeHookDecoratorMetadata<any>> = Reflect.getMetadata(MAKE_HOOK_DECORATOR_METADATA, model);

    if (makeHookDecoratorMetadata) {
        for (const { cb } of makeHookDecoratorMetadata.values()) {
            cb(model, props);
        }
    }

    return model;
};
