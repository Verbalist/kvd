import React from 'react';
import { Container } from 'inversify';
import { DIContext } from '../di/DIContext';

export type CreateContainerCallback = {
    (container: Container): void;
}

export const useContainer = (cb?: CreateContainerCallback): Container => {
    if (cb) {
        return React.useMemo(() => {
            const container = new Container({ defaultScope: 'Singleton' });
            cb(container);
            return container;
        }, []);
    }
    return React.useContext(DIContext);
};
