import React from 'react';
import { Container } from 'inversify';
import { DIContext } from './DIContext';
import { useContainer } from '../hooks/useContainer';

interface IProviderProps {
    container: Container;
}

export const Provider: React.FC<IProviderProps> = ({ children, container }) => {
    const parent = useContainer();
    if (parent) {
        container.parent = parent;
    }
    return (
        <DIContext.Provider value={container}>
            {children}
        </DIContext.Provider>
    );
};
