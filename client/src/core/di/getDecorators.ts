import { injectable, Container } from 'inversify';

export const getDecorators = (container: Container) => {

    const singleton = (target: any) => {
        container.bind(injectable()(target)).toSelf().inSingletonScope();
    };

    const transient = (target: any) => {
        container.bind(injectable()(target)).toSelf().inTransientScope();
    };

    return {
        singleton,
        transient
    }
}
