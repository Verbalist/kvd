import React from 'react';
import { Container } from 'inversify';

export const DIContext = React.createContext<Container>(undefined as any);
