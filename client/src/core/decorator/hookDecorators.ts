import React from 'react';
import { useParams } from 'react-router';
import { makeHookDecorator } from './makeHookDecorator';

export const onInit = makeHookDecorator('onInit', null, (model, props, items) => {
    React.useEffect(() => {
        for (const { property } of items) {
            model[property]();
        }
    }, []);
});

export const onDestroy = makeHookDecorator('onDestroy', null, (model, props, items) => {
    React.useEffect(() => {
        return () => {
            for (const { property } of items) {
                model[property]();
            }
        }
    }, []);
});

export const subscriber = makeHookDecorator('subscriber', null, (model, props, items) => {
    React.useEffect(() => {
        return () => {
            for (const { property } of items) {
                const subscriber = model[property];
                if (subscriber.unsubscribe) {
                    model[property].unsubscribe();
                }
            }
        };
    }, []);
});

export const urlParams = (name?: string) => makeHookDecorator<string | undefined>('urlParams', name, (model, props, items) => {
    const urlParams = useParams<Record<string, any>>();
    for (const { property, meta } of items) {
        const params = meta ? urlParams[meta] : urlParams;
        React.useEffect(() => {
            if (params) {
                model[property](params);
            }
        }, [params]);
    }
});

export const context = (value: any) => makeHookDecorator('context', value, (model, props, items) => {
    for (const { property, meta } of items) {
        model[property] = React.useContext(meta);
    }
});

export const props = (name?: string) => makeHookDecorator('props', name, (model, props, items) => {
    props = props || {};
    for (const { property, meta } of items) {
        model[property] = meta ? props[meta] : props;
    }
});
