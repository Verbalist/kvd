export const storage = (storageKey?: string) => {
    return (target: Object, name: string) => {

        const key = storageKey || `${target.constructor.name}.${name}`;

        Object.defineProperty(target, name, {
            get() {
                return JSON.parse(String(localStorage.getItem(key)));
            },
            set(value: any) {
                localStorage.setItem(key, JSON.stringify(value));
            },
            enumerable: true,
            configurable: true
        });
    }
}
