export const MAKE_HOOK_DECORATOR_METADATA = Symbol.for('MAKE_HOOK_DECORATOR_METADATA');

export type MakeHookDecoratorItem<T> = {
    meta: T;
    property: string;
}

export type MakeHookDecoratorMetadata<T> = {
    items: MakeHookDecoratorItem<T>[];
    cb(model: any, props: any): void;
};

export type MakeHookDecoratorCallback<T> = {
    (model: any, props: any, items: MakeHookDecoratorItem<T>[]): void;
}

export const makeHookDecorator = <T>(key: string, meta: T, cb: MakeHookDecoratorCallback<T>) => {
    return (target: any, property: string) => {
        const metadata: Map<string, MakeHookDecoratorMetadata<T>> = Reflect.getMetadata(MAKE_HOOK_DECORATOR_METADATA, target) || new Map();

        if (!metadata.has(key)) {
            metadata.set(key, {
                items: [],
                cb(model: any, props: any) {
                    const makeHookDecoratorMetadata: Map<string, MakeHookDecoratorMetadata<T>> = Reflect.getMetadata(MAKE_HOOK_DECORATOR_METADATA, model);
                    cb(model, props, makeHookDecoratorMetadata.get(key)!.items);
                }
            });
        }

        metadata.get(key)!.items.push({
            property,
            meta
        });

        Reflect.defineMetadata(MAKE_HOOK_DECORATOR_METADATA, metadata, target);
    }
}
