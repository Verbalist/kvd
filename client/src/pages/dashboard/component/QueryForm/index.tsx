import React from 'react';
import { useStyles } from 'src/pages/dashboard/component/QueryForm/style';
import { QueryFormModel } from 'src/pages/dashboard/component/QueryForm/model';
import { observer } from 'mobx-react-lite';
import AppForm from 'src/ui-kit/AppForm';
import AppTextField from 'src/ui-kit/AppTextField';
import { Query, QueryType } from 'src/model/Query';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import QueryValueForm from 'src/pages/dashboard/component/QueryValueForm';
import { useModel } from 'src/core';

export interface IQueryFormProps {
    onSubmit(query: Query): void;
}

const QueryForm: React.FC<IQueryFormProps> = (props) => {
    const classes = useStyles({});
    const model = useModel(QueryFormModel, props);

    return (
        <AppForm model={model.query} onSubmit={model.onSubmit}>
            <div className={classes.head}>
                <AppTextField
                    label='Type'
                    name='type'
                    className={classes.type}
                    select
                >
                    {Object.entries(QueryType).map(([label, key]) => (
                        <MenuItem key={key} value={key}>{label}</MenuItem>
                    ))}
                </AppTextField>
                <AppTextField
                    label='Url'
                    name='url'
                    className={classes.url}
                />
                <Button
                    color='primary'
                    variant='contained'
                    className={classes.send}
                    type='submit'
                >
                    Send
                </Button>
            </div>
            <QueryValueForm query={model.query} />
        </AppForm>
    )
};

export default observer(QueryForm);
