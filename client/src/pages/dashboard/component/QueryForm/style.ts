import { makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
    },
    head: {
        display: 'flex',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        alignItems: 'flex-start'
    },
    type: {
        width: 140,
    },
    url: {
        width: '100%'
    },
    send: {
        marginTop: '16.5px!important'
    }
}));
