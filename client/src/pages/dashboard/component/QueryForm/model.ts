import { observable, action } from 'mobx';
import { singleton } from 'src/container';
import { Query } from 'src/model/Query';
import { IQueryFormProps } from 'src/pages/dashboard/component/QueryForm';
import { props } from 'src/core';

@singleton
export class QueryFormModel {

    @observable query: Query;

    @props()
    props!: IQueryFormProps;

    constructor() {
        this.query = new Query();
    }

    @action.bound onSubmit() {
        this.props.onSubmit(this.query);
    }

}
