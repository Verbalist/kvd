import { observable, reaction, action } from 'mobx';
import { singleton } from 'src/container';
import { Query } from 'src/model/Query';
import { QueryValuePairs, QueryValuePair } from 'src/model/QueryValue';
import { props, onInit } from 'src/core';
import { IQueryValuePairsFormProps } from 'src/pages/dashboard/component/QueryValueForm/QueryValuePairsForm';

@singleton
export class QueryValuePairsFormModel {

    query: Query;

    @observable data: QueryValuePairs;

    @props()
    props!: IQueryValuePairsFormProps;

    constructor() {
        this.query = new Query();
        this.data = new QueryValuePairs();

        reaction(() => this.data.value, () => {
            this.query.value = this.data.value;
        });
    }

    @onInit
    onInit() {
        this.query = this.props.query;
        this.data.clear();
    }

    @action.bound onAdd() {
        this.data.items.push(new QueryValuePair());
    }

    @action.bound onDelete(data: QueryValuePair) {
        this.data.items = this.data.items.filter(item => item !== data);
    }
}
