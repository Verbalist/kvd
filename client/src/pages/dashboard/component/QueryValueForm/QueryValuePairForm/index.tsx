import React from 'react';
import { useStyles } from 'src/pages/dashboard/component/QueryValueForm/QueryValuePairForm/style';
import { QueryValuePairFormModel } from 'src/pages/dashboard/component/QueryValueForm/QueryValuePairForm/model';
import { observer } from 'mobx-react-lite';
import AppForm from 'src/ui-kit/AppForm';
import AppTextField from 'src/ui-kit/AppTextField';
import { Query } from 'src/model/Query';
import { useModel } from 'src/core/hooks/useModel';

export interface IQueryValuePairFormProps {
    query: Query;
}

const QueryValuePairForm: React.FC<IQueryValuePairFormProps> = (props) => {
    const classes = useStyles({});
    const model = useModel(QueryValuePairFormModel, props);

    return (
        <AppForm model={model.data} component='div'>
            <div>
                <AppTextField
                    label='Key'
                    name='pairKey'
                />
                <AppTextField
                    label='Value'
                    name='pairValue'
                    className={classes.value}
                />
            </div>
        </AppForm>
    )
};

export default observer(QueryValuePairForm);
