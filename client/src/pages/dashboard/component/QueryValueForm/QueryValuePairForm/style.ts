import { makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
    },
    value: {
        marginLeft: 20
    }
}));
