import { observable, reaction } from 'mobx';
import { singleton } from 'src/container';
import { Query } from 'src/model/Query';
import { QueryValuePair } from 'src/model/QueryValue';
import { props, onInit } from 'src/core';
import { IQueryValuePairFormProps } from 'src/pages/dashboard/component/QueryValueForm/QueryValuePairForm';

@singleton
export class QueryValuePairFormModel {

    query: Query;

    @observable data: QueryValuePair;

    @props()
    props!: IQueryValuePairFormProps;

    constructor() {
        this.query = new Query();
        this.data = new QueryValuePair();

        reaction(() => this.data.value, () => {
            this.query.value = this.data.value;
        });
    }

    @onInit
    onInit() {
        this.query = this.props.query;
        this.data.clear();
    }
}
