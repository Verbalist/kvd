import React from 'react';
import { observer } from 'mobx-react-lite';
import { Query, QueryType } from 'src/model/Query';
import QueryValueKeyForm from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeyForm';
import QueryValueKeysForm from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeysForm';
import QueryValuePairForm from 'src/pages/dashboard/component/QueryValueForm/QueryValuePairForm';
import QueryValuePairsForm from 'src/pages/dashboard/component/QueryValueForm/QueryValuePairsForm';

interface IQueryValueFormProps {
    query: Query;
}

const QueryValueForm: React.FC<IQueryValueFormProps> = ({ query }) => {
    switch (query.type) {
        case QueryType.GET:
        case QueryType.DELETE:
            return <QueryValueKeyForm key={query.type} query={query} />;

        case QueryType.MGET:
        case QueryType.MDELETE:
            return <QueryValueKeysForm key={query.type} query={query} />;

        case QueryType.SET:
            return <QueryValuePairForm key={query.type} query={query} />;

        case QueryType.MSET:
            return <QueryValuePairsForm key={query.type} query={query} />;

        default:
            return null
    }
};

export default observer(QueryValueForm);
