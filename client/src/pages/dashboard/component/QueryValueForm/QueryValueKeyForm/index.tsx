import React from 'react';
import { useStyles } from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeyForm/style';
import { QueryValueKeyFormModel } from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeyForm/model';
import { observer } from 'mobx-react-lite';
import AppForm from 'src/ui-kit/AppForm';
import AppTextField from 'src/ui-kit/AppTextField';
import { Query } from 'src/model/Query';
import { useModel } from 'src/core/hooks/useModel';

export interface IQueryValueKeyFormProps {
    query: Query;
}

const QueryValueKeyForm: React.FC<IQueryValueKeyFormProps> = (props) => {
    const classes = useStyles({});
    const model = useModel(QueryValueKeyFormModel, props);

    return (
        <AppForm model={model.data} component='div'>
            <div>
                <AppTextField
                    label='Key'
                    name='value'
                />
            </div>
        </AppForm>
    )
};

export default observer(QueryValueKeyForm);
