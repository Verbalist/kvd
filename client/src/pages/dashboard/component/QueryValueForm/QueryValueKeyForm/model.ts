import { observable, reaction } from 'mobx';
import { singleton } from 'src/container';
import { Query } from 'src/model/Query';
import { QueryValueKey } from 'src/model/QueryValue';
import { props, onInit } from 'src/core';
import { IQueryValueKeyFormProps } from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeyForm';

@singleton
export class QueryValueKeyFormModel {

    query: Query;

    @observable data: QueryValueKey;

    @props()
    props!: IQueryValueKeyFormProps;

    constructor() {
        this.query = new Query();
        this.data = new QueryValueKey();

        reaction(() => this.data.value, () => {
            this.query.value = this.data.value;
        });
    }

    @onInit
    onInit() {
        this.query = this.props.query;
        this.data.clear();
    }
}
