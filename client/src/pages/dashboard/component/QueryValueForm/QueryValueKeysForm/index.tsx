import React from 'react';
import { useStyles } from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeysForm/style';
import { QueryValueKeysFormModel } from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeysForm/model';
import { observer } from 'mobx-react-lite';
import AppForm from 'src/ui-kit/AppForm';
import AppTextField from 'src/ui-kit/AppTextField';
import { Query } from 'src/model/Query';
import { useModel } from 'src/core/hooks/useModel';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

export interface IQueryValueKeysFormProps {
    query: Query;
}

const QueryValueKeysForm: React.FC<IQueryValueKeysFormProps> = (props) => {
    const classes = useStyles({});
    const model = useModel(QueryValueKeysFormModel, props);

    return (
        <div className={classes.root}>
            <Fab
                color='primary'
                size='small'
                onClick={model.onAdd}
            >
                <AddIcon />
            </Fab>
            <div className={classes.items}>
                {model.data.items.map((item, index) => (
                    <AppForm model={item} key={index} component='div'>
                        <div className={classes.item}>
                            <AppTextField
                                label='Key'
                                name='value'
                            />
                            <Fab
                                color='secondary'
                                size='small'
                                onClick={() => model.onDelete(item)}
                                className={classes.delete}
                            >
                                <RemoveIcon />
                            </Fab>
                        </div>
                    </AppForm>
                ))}
            </div>
        </div>
    )
};

export default observer(QueryValueKeysForm);
