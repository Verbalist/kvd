import { observable, reaction, action } from 'mobx';
import { singleton } from 'src/container';
import { Query } from 'src/model/Query';
import { QueryValueKeys, QueryValueKey } from 'src/model/QueryValue';
import { props, onInit } from 'src/core';
import { IQueryValueKeysFormProps } from 'src/pages/dashboard/component/QueryValueForm/QueryValueKeysForm';

@singleton
export class QueryValueKeysFormModel {

    query: Query;

    @observable data: QueryValueKeys;

    @props()
    props!: IQueryValueKeysFormProps;

    constructor() {
        this.query = new Query();
        this.data = new QueryValueKeys();

        reaction(() => this.data.value, () => {
            this.query.value = this.data.value;
        });
    }

    @onInit
    onInit() {
        this.query = this.props.query;
        this.data.clear();
    }

    @action.bound onAdd() {
        this.data.items.push(new QueryValueKey());
    }

    @action.bound onDelete(data: QueryValueKey) {
        this.data.items = this.data.items.filter(item => item !== data);
    }
}
