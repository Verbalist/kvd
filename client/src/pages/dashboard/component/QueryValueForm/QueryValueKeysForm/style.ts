import { makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {

    },
    items: {
        marginTop: 10
    },
    item: {
        display: 'flex',
        alignItems: 'center'
    },
    delete: {
        marginLeft: 20
    }
}));
