import React from 'react';
import { DashboardPageModel } from 'src/pages/dashboard/component/DashboardPage/model';
import { useModel } from 'src/core';
import Paper from '@material-ui/core/Paper';
import { useStyles } from 'src/pages/dashboard/component/DashboardPage/style';
import QueryForm from 'src/pages/dashboard/component/QueryForm';
import ResultPanel from 'src/pages/dashboard/component/ResultPanel';
import { observer } from 'mobx-react-lite';
import { Progress } from 'src/ui-kit/Progress';
import ErrorAlert from 'src/app/component/ErrorAlert';

interface IDashboardPageProps {
}

const DashboardPage: React.FC<IDashboardPageProps> = () => {
    const classes = useStyles({});
    const model = useModel(DashboardPageModel);

    return (
        <div className={classes.root}>
            <Progress isLoading={model.isLoading} />
            <QueryForm onSubmit={model.onSubmit} />
            <ErrorAlert errors={model.errors} className={classes.errors} />
            {model.result ? (
                <Paper elevation={2} className={classes.result}>
                    <ResultPanel value={model.result} />
                </Paper>
            ) : null}
        </div>
    );
};

export default observer(DashboardPage);
