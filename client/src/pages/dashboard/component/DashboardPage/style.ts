import { makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(1),
        height: '100%'
    },
    grid: {
        height: '100%',
    },
    content: {
        height: '100%'
    },
    submit: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    result: {
        marginTop: 50
    },
    errors: {
        marginTop: 50
    },
}));
