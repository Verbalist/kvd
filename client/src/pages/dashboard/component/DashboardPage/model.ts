import { singleton } from 'src/container';
import { observable, action, toJS } from 'mobx';
import { Query } from 'src/model/Query';
import { APIService } from 'src/app/service/APIService';
import { getErrors } from 'src/util';

@singleton
export class DashboardPageModel {

    @observable result?: object;

    @observable isLoading: boolean = false;

    @observable errors: string[] = [];

    constructor(
        private readonly apiService: APIService
    ) { }

    @action.bound async onSubmit(query: Query) {
        this.isLoading = true;
        this.result = undefined;

        try {
            this.result = await this.apiService.send(toJS(query));
        } catch (error) {
            this.errors = getErrors(error);
        } finally {
            this.isLoading = false;
        }
    }
}
