import React from 'react';
import { useStyles } from 'src/pages/dashboard/component/ResultPanel/style';

interface IResultPanelProps {
    value: object;
}

const ResultPanel: React.FC<IResultPanelProps> = ({ value }) => {
    const classes = useStyles({});

    return (
        <div className={classes.root}>
            <pre>
                {JSON.stringify(value, null, '\t')}
            </pre>
        </div>
    );
};

export default ResultPanel;
