import React, { ReactNode } from 'react';
import { AppFormContext, AppFormContextValue } from './context';
import { observer } from 'mobx-react-lite';

interface IAppFormProps {
    children: ReactNode;
    className?: string;
    model: any;
    onSubmit?: () => void;
    component?: React.ElementType<React.HTMLAttributes<HTMLElement>>;
}

const AppForm: React.FC<IAppFormProps> = ({ children, ...props }) => {
    const context = React.useContext(AppFormContext);
    const value = React.useMemo(() => new AppFormContextValue(props.model), [props.model]);
    const Component = props.component || 'form';

    const onSubmit = React.useCallback(async (event: React.FormEvent) => {
        event.stopPropagation();
        event.preventDefault();

        const isValid = await value.isValid();
        if (isValid && props.onSubmit) {
            props.onSubmit();
        }

    }, [value]);

    if (context) {
        React.useEffect(() => {
            context.addChildren(value);
            return () => context.deleteChildren(value);
        }, [value]);
    }

    return (
        <AppFormContext.Provider value={value}>
            <Component className={props.className} onSubmit={onSubmit} noValidate>
                {children}
            </Component>
        </AppFormContext.Provider>
    );
};

export default observer(AppForm);