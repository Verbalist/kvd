import React from 'react';
import { observable, action } from 'mobx';
import { validate } from 'class-validator'

export class AppFormContextValue<T = any> {
    @observable model: T;
    @observable errors: Record<string, string[]>;

    private children: Set<AppFormContextValue>;

    constructor(model: T) {
        this.model = model;
        this.errors = {};
        this.children = new Set();
    }

    addChildren(value: AppFormContextValue) {
        this.children.add(value);
    }

    deleteChildren(value: AppFormContextValue) {
        this.children.delete(value);
    }

    @action async isValid(): Promise<boolean> {
        const data = await validate(this.model);
        const errors = data.map(item => [item.property, Object.values(item.constraints)]);

        this.errors = Object.fromEntries(errors);

        const children = await Promise.all([...this.children].map(item => item.isValid()));

        return errors.length === 0 && children.every(Boolean);
    }
}

export const AppFormContext = React.createContext<AppFormContextValue<any>>(null as any);
