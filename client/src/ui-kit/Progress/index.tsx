import React from 'react';
import { useStyles } from 'src/ui-kit/Progress/style';
import { observer } from 'mobx-react-lite';
import LinearProgress, { LinearProgressProps } from '@material-ui/core/LinearProgress';


interface IProgressProps extends LinearProgressProps {
    isLoading?: boolean;
}

export const Progress: React.FC<IProgressProps> = observer(({ isLoading }) => {
    const classes = useStyles({});

    return (
        <div className={classes.root}>
            {isLoading ? (
                <LinearProgress />
            ) : null}
        </div>
    );
});
