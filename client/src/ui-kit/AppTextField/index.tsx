import React from 'react';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import { AppFormContext } from '../AppForm/context';
import { observer } from 'mobx-react-lite';

export type AppTextFieldProps = TextFieldProps & {
    name: string;
}
const AppTextField: React.FC<AppTextFieldProps> = (props) => {
    const [errors, setErrors] = React.useState<string[]>([]);
    const form = React.useContext(AppFormContext);
    const name = props.name;

    const onChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setErrors([]);
        form.model[name] = event.target.value;
    }

    React.useEffect(() => {
        setErrors(form.errors[name] || []);
    }, [form.errors[name]])

    return (
        <TextField
            onChange={onChange}
            error={Boolean(errors.length)}
            helperText={errors.join()}
            value={form.model[name]}
            variant='outlined'
            size='small'
            margin='normal'
            {...props}
        >
            {props.children}
        </TextField>
    );
};

export default observer(AppTextField);
