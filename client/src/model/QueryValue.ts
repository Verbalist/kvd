import { observable, computed } from 'mobx';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { QueryKey, QueryValue, QueryPair } from 'src/model/Query';

export interface IQueryValue<T> {
    clear(): void;
    value: T;
}

export class QueryValueKey implements IQueryValue<QueryKey> {

    @IsNotEmpty()
    @observable value: QueryKey;

    constructor() {
        this.value = ''
    }

    clear() {
        this.value = '';
    }
}

export class QueryValueKeys implements IQueryValue<QueryKey[]> {

    @ValidateNested({
        each: true
    })
    @observable items: QueryValueKey[];

    constructor() {
        this.items = [];
    }

    clear() {
        this.items = [];
    }

    @computed get value(): QueryKey[] {
        return this.items.map(item => item.value);
    }
}

export class QueryValuePair implements IQueryValue<QueryPair> {

    @IsNotEmpty()
    @observable pairKey: QueryKey;

    @IsNotEmpty()
    @observable pairValue: QueryValue;

    constructor() {
        this.pairKey = '';
        this.pairValue = '';
    }

    clear() {
        this.pairKey = '';
        this.pairValue = '';
    }

    @computed get value(): QueryPair {
        return [this.pairKey, this.pairValue];
    }
}


export class QueryValuePairs implements IQueryValue<QueryPair[]> {

    @ValidateNested({
        each: true
    })
    @observable items: QueryValuePair[];

    constructor() {
        this.items = [];
    }

    clear() {
        this.items = [];
    }

    @computed get value(): QueryPair[] {
        return this.items.map(item => item.value);
    }
}
