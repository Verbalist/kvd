export type ResponseEntity<T = any> = {
    success: boolean;
    errors?: string[];
    data?: T;
}