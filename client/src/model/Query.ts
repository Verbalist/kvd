import { observable } from 'mobx';
import { IsUrl, IsNotEmpty } from 'class-validator';

export enum QueryType {
    GET = 'get',
    MGET = 'mget',
    SET = 'set',
    MSET = 'mset',
    DELETE = 'delete',
    MDELETE = 'mdelete',
}

export type QueryKey = string;

export type QueryValue = string | number;

export type QueryPair = [QueryKey, QueryValue];

export type QueryRaw = Partial<Query>;

export class Query {

    @IsUrl()
    @IsNotEmpty()
    @observable url: string;

    @observable type: QueryType;

    @observable value: QueryKey | QueryKey[] | QueryPair | QueryPair[];

    constructor(raw: QueryRaw = {}) {
        this.url = raw.url || '';
        this.type = raw.type || QueryType.GET;
        this.value = raw.value || '';
    }
}
