import { singleton } from 'src/container';
import { HttpService } from 'src/app/service/HttpService';
import { Query } from 'src/model/Query';
import { ResponseEntity } from 'src/model/ResponseEntity';

@singleton
export class APIService {

    constructor(
        private readonly httpService: HttpService
    ) { }

    async send(query: Query) {
        const { url, ...request } = query;

        return await this.httpService.post<ResponseEntity>(url, request);
    }

}