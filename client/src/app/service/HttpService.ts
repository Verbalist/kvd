import { singleton } from 'src/container';

@singleton
export class HttpService {
    async post<T>(url: string, data?: object): Promise<T> {
        const responce = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return await this.responce(responce);
    }

    private async responce(responce: Response) {
        const json = await responce.json();
        if (!responce.ok) {
            throw new Error(json.message);
        }
        return json;
    }
}
