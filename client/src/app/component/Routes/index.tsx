import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import DashboardPage from 'src/pages/dashboard/component/DashboardPage';

interface IRoutesProps {
}

const Routes: React.FC<IRoutesProps> = () => {

    return (
        <Switch>
            <Route exact path='/dashboard' component={DashboardPage} />
            <Redirect to='/dashboard' />
        </Switch>
    )
};

export default Routes;
