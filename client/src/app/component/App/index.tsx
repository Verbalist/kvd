import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'src/core';
import { container } from 'src/container';
import Theme from 'src/app/component/Theme';
import Routes from 'src/app/component/Routes';
import AppNavbar from 'src/app/component/AppNavbar';
import { useStyles } from 'src/app/component/App/style';
import Container from '@material-ui/core/Container';

const App: React.FC = () => {
    const classes = useStyles({});

    return (
        <Container>
            <Provider container={container}>
                <Theme>
                    <Router>
                        <AppNavbar />
                        <main className={classes.main}>
                            <Routes />
                        </main>
                    </Router>
                </Theme>
            </Provider>
        </Container>
    )
};

export default App;
