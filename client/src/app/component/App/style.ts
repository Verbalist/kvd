import { makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    main: {
        height: 'calc(100vh - 64px)'
    },
}));
