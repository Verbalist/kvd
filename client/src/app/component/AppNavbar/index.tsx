import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { useStyles } from 'src/app/component/AppNavbar/style';
import { observer } from 'mobx-react-lite';
import Typography from '@material-ui/core/Typography';

interface IAppNavbarProps {
}

const AppNavbar: React.FC<IAppNavbarProps> = () => {
    const classes = useStyles({});

    return (
        <AppBar position='static'>
            <Toolbar>
                <Typography variant='h6'>
                    key-value db
                </Typography>
            </Toolbar>
        </AppBar>
    )
};

export default observer(AppNavbar);
