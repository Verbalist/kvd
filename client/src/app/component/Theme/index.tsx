import React from 'react';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import { theme } from 'src/app/component/Theme/theme';

const Theme: React.FC = ({ children }) => {

    return (
        <ThemeProvider theme={theme}>
            {children}
        </ThemeProvider>
    )
};

export default Theme;
