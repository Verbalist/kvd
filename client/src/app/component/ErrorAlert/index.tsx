import React from 'react';
import { useStyles } from 'src/app/component/ErrorAlert/style';
import { observer } from 'mobx-react-lite';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import clsx from 'clsx';

interface IErrorAlertProps {
    errors?: string[];
    className?: string;
}

export const ErrorAlert: React.FC<IErrorAlertProps> = ({ errors, className }) => {
    const classes = useStyles({});

    if (Array.isArray(errors) && errors.length !== 0) {
        return (
            <Alert severity='error' className={clsx(classes.root, className)}>
                <AlertTitle>Error</AlertTitle>
                {errors.join('<br>')}
            </Alert>
        )
    }

    return null;
};

export default observer(ErrorAlert);
