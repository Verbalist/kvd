import 'reflect-metadata';
import React from 'react';
import ReactDOM from 'react-dom';
import App from 'src/app/component/App';

ReactDOM.render((
    <App />
), document.getElementById('app'));
