import { Container } from 'inversify';
import { getDecorators } from 'src/core';

export const container = new Container({ defaultScope: 'Singleton' });

const { singleton, transient } = getDecorators(container);

export {
    singleton,
    transient
};
