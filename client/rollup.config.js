import typescript from 'rollup-plugin-typescript2';
import commonjs from '@rollup/plugin-commonjs';
import external from 'rollup-plugin-peer-deps-external';
import replace from '@rollup/plugin-replace';
import resolve from '@rollup/plugin-node-resolve';
import url from '@rollup/plugin-url';
import { sizeSnapshot } from 'rollup-plugin-size-snapshot';

const NODE_ENVS = {
    PROD: 'production',
    DEV: 'development'
}

const NODE_ENV = process.env.NODE_ENV || NODE_ENVS.DEV;

const plugins = async env => {
    if (env === NODE_ENVS.DEV) {
        const { default: serve } = await import('rollup-plugin-serve');
        const { default: livereload } = await import('rollup-plugin-livereload');

        return [
            serve({
                contentBase: ['public'],
                host: '0.0.0.0',
                port: process.env.CLIENT_PORT,
                historyApiFallback: true
            }),
            livereload({
                watch: 'public/dist',
                port: 35729
            })
        ]
    }

    if (env === NODE_ENVS.PROD) {
        const { terser } = await import('rollup-plugin-terser');

        return [
            terser()
        ]
    }

    return [];
}

export default (async () => ({
    input: 'src/index.tsx',
    output: [
        {
            file: 'public/dist/index.js',
            format: 'iife',
            sourcemap: NODE_ENV === NODE_ENVS.DEV,
        }
    ],
    external: [
        'react',
        'react-dom',
    ],
    treeshake: NODE_ENV === NODE_ENVS.PROD,
    plugins: [
        replace({
            'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
        }),
        resolve({
            preferBuiltins: true
        }),
        external(),
        url(),
        typescript({
            rollupCommonJSResolveHack: true,
            clean: true
        }),
        commonjs({
            include: /node_modules/,
            namedExports: {
                'node_modules/react-is/index.js': [
                    'ForwardRef',
                    'isFragment',
                    'isLazy',
                    'isMemo',
                    'Memo',
                    'isValidElementType',
                ],
                'node_modules/prop-types/index.js': [
                    'elementType'
                ],
                'node_modules/class-validator/index.js': [
                    'IsString',
                    'IsNotEmpty',
                    'IsUrl',
                    'ValidateNested'
                ]
            }
        }),
        sizeSnapshot(),
        ...await plugins(NODE_ENV)
    ]
}))();
